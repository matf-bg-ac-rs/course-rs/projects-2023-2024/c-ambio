Opis slučaja upotrebe "Igranje jedne partije igre":
Kratak opis:  Igranje igre započinje igrač, klikom na dugme "Započni igru" iz glavnog menija aplikacije. Aplikacija aktivira igru. Igra počinje u trenutku kada igrač koji se prvi priključio igri pritisne na dugme "Pridruži se igri", pod uslovom da se priključio dovoljan broj igrača. Igranje partije započinje tako što se svim igračima dodele po četiri karte. Svaki igrač na početku partije ima pravo da pogleda i zapamti dve od četiri dobijene karte. Tokom igre igrač izvlači karte sa vrha špila ili talona da bi zamenio sa nekom od svojih karata ili iskoristio efekat magične karte, i  tako potencijalno umanjio ukupan zbir svojih karata. Dodatno svaki igrač može da u toku igre na talon spusti neku od svojih ili protivnikovih karata, kako bi smanjio broj karata koje poseduje u ruci. U trenutku kada neko od igrača pozove "Kambio", odigrava se još jedan krug igre, pri čemu bilo kakav efekat magije ne utiče na igrača koji je pozvao "Kambio". Kada ponovo na potezu bude igrač koji je pozvao "Kambio", partija se završava i prikazuje se pobednik i rezultati partije.

Akteri: 
	- Igrač

Preduslovi: Aplikacija je pokrenuta i prikazuje glavni meni.
Postuslovi: Informacije o odigranoj partiji su trajno sačuvane i prikazane.

Osnovni tok:
1. Igrač bira dugme "Započni igru" iz glavnog menija.
2. Prelazi se na slučaj upotrebe "Pridruživanje igrača". Po završetku prelazi se na korak 3.
3. Igra kreira spil karata za igru.
4. Igra kreira talon karata za igru i postavlja prvu kartu na talon.
5. Igra dodeljuje karte igračima.
6. Igra šalje informaciju Igracima o dodeljenim kartama, prvoj karti na vrhu talona, kao i igraca koji je na potezu.
7. Sve dok neko od igraca ne obavesti Igru da je spreman, Igracu se prikazuju prve dve karte. 
8. Pritiskom na dugme "Spreman": 
	8.1. Igra okrece karte Igracima i salje poruku da mogu da zapocnu igranje igre.
9. Sve dok Igrac koji je pritisnuo dugme  "Kambio" ne bude ponovo na potezu:
	9.1. Igra obaveštava Igrače koji je Igrač trenutno na potezu.
	9.2. Igra osluškuje koji od Igrača je prvi povukao potez.
		9.2.1. Ako je bilo koji Igrač odabrao neku od karata koje nisu ni na vrhu talona ni na vrhu spila(događaj "Uletanja").
			9.2.1.1. Prelazi se na podtok P2. Po završetku prelazi se na 9.2.
		9.2.2. Ako je Igrač koji je na potezu izvukao kartu sa vrha špila ili sa vrha talona:
			9.2.2.1. Ako je Igrač na potezu izvukao kartu sa vrha špila:
				Prelazi se na podtok P1. Po završetku prelazi se na sledecu iteraciju petlje(korak 9. osnovnog toka).
			9.2.2.2. Ako je Igrač na potezu izvukao kartu sa vrha talona:
				9.2.2.2.1. Igra prikazuje Igraču dugme "Kambio"
				9.2.2.2.2. Igra zahteva od Igrača na potezu da izabere neku od svojih karata.
				9.2.2.2.3. Igrač obaveštava Igru sa kojom od svojih karata želi da izvrši zamenu.
				9.2.2.2.4. Ako je Igrac pritisnuo dugme "Kambio":
					9.2.2.2.4.1. Igra označava Igrača koji je pritisnuo dugme "Kambio"
					9.2.2.2.4.2. Prelazi se na tacku 9. osnovnog toka, uz napomenu da je Igrač koje je pozvao "Kambio" izuzet iz nastavka igre.
10. Igra prikazuje rezultate trenutne partije i dugme za povratak u glavni meni.
11. Prelazi se na slucaj upotrebe "Cuvanje rezultata"
					
Alternativni tokovi: -A1: "Neočekivani izlaz iz aplikacije". Ukoliko u bilo kojem koraku korisnik isključi aplikaciju, sve eventualne zapamćene informacije o trenutnoj partiji igre se poništavaju i aplikacija završava rad. Slučaj upotrebe se završava. 


Podtokovi:

	-P1: "Izvlacenje karte sa vrha spila":
1. Igra prikazuje Igraču izvučenu kartu sa vrha špila i dugme "Kambio".
	1.1. Ako je Igrač izvukao Magičnu kartu:
		1.1.1. Igrač bira klikom na talon ili klikom na neku od svojih karata akciju za završetak svog poteza.
			1.1.1.1. Ako je Igrač odabrao talon:
				1.1.1.1.1. Igrač može iskoristiti efekat Magične karte(dodatne informacije D1).
			1.1.1.2. Ako je Igrač odabrao neku od svojih karata:
				1.1.1.2.1. Igra vrši zamenu izvučene karte sa odabranom kartom.
	1.2. Ako je Igrač izvukao regularnu kartu:
		1.2.1. Igrač bira klikom na talon ili na neku od svojih karata akciju za završetak svog poteza.
	1.3. Ako je Igrač pritisnuo dugme "Kambio":
		1.3.1. Igra označava Igrača koji je pritisnuo dugme "Kambio"
		1.3.2. Prelazi se na tacku 5. osnovnog toka, uz napomenu da je Igrač koje je pozvao "Kambio" izuzet iz nastavka igre.




	-P2: "Uletanje":
1. Ako je Igrač odabrao neku od svojih karata:
	1.1.Igra proverava da li se izabrana karta poklapa sa vrhom talona:
		1.1.1. Ako se poklapaju karta sa vrha talona i izabrana karta:
			1.1.1.1. Igrač se rešio izabrane karte, čime je smanjio sebi ukupan zbir.
		1.1.2. Ako se ne poklapaju karta sa vrha talona i izabrana karta:
			1.1.2.1. Igra dodeljuje Igraču, za kaznu, jednu dodatnu kartu sa vrha špila.
2. Ako je Igrač odabrao neku od karata drugih igrača:
	2.1. Aplikacija provera da li se izabrana karta poklapa sa vrhom talona:
		2.1.1. Ako se poklapaju karta sa vrha talona i izabrana karta:
			2.1.1.1. Igrač bira među svojim kartama, kartu koju želi da dodeli drugom Igraču čiju kartu je izabrao.
		2.1.2. Ako se ne poklapaju karta sa vrha talona i izabrana karta:
			2.1.2.1. Igra dodeljuje Igraču, za kaznu, jednu dodatnu kartu sa vrha talona.
3. Igra proverava da li je registrovala još neki događaj "Uletanja":
	3.1. Ako je registrovan drugi dogadjaj "Uletanja":
		3.1.1. Igra dodeljuje kaznu, dodatnu kartu sa vrha špila, Igraču koji je drugi pokrenuo događaj "Uletanja"
	


Specijalni zahtevi:/
Dodatne informacije: -D1: "Efekat Magične karte": U zavisnosti od izvučene Magične karte, igraču se uslovljava koji naredni potez može da odigra. Ako je Igrač izvukao kartu:
			-EYE -> Igraču se pruža mogućnost da pogleda neku od svojih karata koje ima u ruci.
			-DETECTIVE -> Igraču se pruža mogućnost da pogleda neku od karata koje protivnik ima u ruci.
			-SWAP -> Igraču se pruža mogućnost da odabere dve karte i zameni ih, bez prethodnog gledanja u karte kojima vrši zamenu. Potrebno je da izabere dva igrača da im zameni po jednu kartu u ruci ili da odabere neku svoju kartu iz ruke i zameni je sa nekom od protivnikovih.
			-LOOKandSWAP -> Igraču se pruža mogućnost da izabere neku od protivnikovih karata, pogleda i, po želji, zameni je sa nekom svojom.
			

