Opis slučaja upotrebe "Pridruživanje igrača"

Kratak opis: Igrač upisuje svoje ime, bira svog avatara i bira kada je spreman za povezivanje sa ostalim igračima

Akteri: Igrači  

Preduslovi: Igrači su pokrenuli aplikaciju i iz glavnog menija odabrali opciju "Započni igru"

Postuslovi: Aplikacija je sačuvala prikupljene informacije o igračima 

Osnovni tok: 
1. Aplikacija prikazuje prozor za podešavanje imena i biranje avatara.
2. Igrač je pritisnuo dugme "Pridruži se igri"
    2.1. Ako je Igrač upisao svoje ime i odabrao svog avatara:
        2.1.1. Ako aplikacija već ima sačuvanog igrača sa datim imenom
        Aplikacija čuva ime i avatara Igrača. Prelazi se na korak 3.
    2.2. Ako je Igrač upisao svoje ime, ali nije odabrao svog avatara:
        2.2.1. Aplikacija čuva ime Igrača i dodeljuje mu nasumičnog avatara. Prelazi se na korak 3.
    2.3. Ako Igrač nije upisao ništa u polje za ime, ali je odabrao svog avatara:
        2.3.1. Aplikacija čuva informaciju o avataru Igrača i dodeljuje mu nasumično ime. Prelazi se na korak 3.
    2.4. Ako Igrač nije upisao ništa u polje za ime i nije izabrao svog avatara:
        2.4.1. Aplikacija Igraču dodeljuje nasumično ime i nasumičnog avatara. Prelazi se na korak 3.
3. Klijent šalje serveru TCP zahtev za povezivanje.
4. Server odobrava zahtev za povezivanje klijenta pod uslovom da je ime Igrača nije već izabrano. 
    4.1 Ako je Igrač prvi klijent koji se konektovao na server:
        4.1.1. Igraču se dodeljuje uloga "domaćina". Prelazi se na korak 5.
5. Uspostavlja se TCP konekcija i otvara se soba za čekanje
6. Ako je Igrač "domaćin":
    6.1. Igrač bira opciju "Pokreni igru" i pokreće se igra sa svim igračima koji se nalaze u sobi za čekanje

Alternativni tokovi: 
- A1: "Neočekivani izlaz iz aplikacije": Ukoliko u bilo kom koraku korisnik prekine aplikaciju, sve zapamćene informacije se poništavaju i aplikacija završava rad. Slučaj upotrebe je završen.

Podtokovi: /

Specijalni zahtevi: /

Dodatne informacije: /
