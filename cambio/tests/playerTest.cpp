#include "catch.hpp"
#include "../src/game/player.h"
#include<QVector>
#include<QString>

class Card;

TEST_CASE("Testiranje klase Player i osnovnih funkcionalnosti", "[function]")
{

    SECTION("Da li pri konstrukciji igrača na osnou imena, postavlja odgovarajuća polja")
    {
        //Arrange
        QString expected_name = "Luna";
        int expected_score = 0;
        bool expected_said_cambio = false;
        bool expected_turn = false;

        //Act
        Player player{expected_name};

        //Assert
        REQUIRE(expected_name == player.name());
        REQUIRE(expected_score == player.score());
        REQUIRE(expected_said_cambio == player.saidCambio());
        REQUIRE(expected_turn == player.myTurn());
    }

    SECTION("Da li funkcija calculateScore računa odgovarajući score igrača")
    {
        //Arrange
        QString expected_name = "Luna";
        Player player{expected_name};
        int expected_score = 16;
        QVector<Card*> cards;
        cards.push_back(Card::createCard(7));
        cards.push_back(Card::createCard(3));
        cards.push_back(Card::createCard(2));
        cards.push_back(Card::createCard(4));
        player.setHand(cards);

        //Act
        player.calculateScore();

        //Assert
        REQUIRE(expected_score == player.score());
    }
    SECTION("Da li funkcija changeCardAtIndex zameni igrgraču kartu na osgovarajućoj poziciji")
    {
        //Arrange
        QString expected_name = "Luna";
        Player player{expected_name};

        Card* card0 = Card::createCard(7);
        Card* card1 = Card::createCard(8);
        Card* card2 = Card::createCard(5);
        Card* card3 = Card::createCard(6);
        Card* card4 = Card::createCard(1);

        QVector<Card*> cards;
        cards.push_back(card0);
        cards.push_back(card1);
        cards.push_back(card2);
        cards.push_back(card3);
        cards.push_back(card4);

        player.setHand(cards);

        Card* expected_old_card = card2;
        Card* new_card = Card::createCard(15);

        QVector<Card*> expected_hand ;
        expected_hand.push_back(card0);
        expected_hand.push_back(card1);
        expected_hand.push_back(new_card);
        expected_hand.push_back(card3);
        expected_hand.push_back(card4);

        //Act
        Card* old_card = player.changeCardAtIndex(new_card, 2);

        //Assert
        REQUIRE(expected_old_card ==old_card);
        REQUIRE(expected_hand ==player.hand());
    }
}
