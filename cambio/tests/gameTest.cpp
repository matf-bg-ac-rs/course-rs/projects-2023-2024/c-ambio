#include "catch.hpp"
#include "../src/server/game.h"
#include<QVector>
#include<QString>

class Card;

TEST_CASE("Testiranje klase Game i osnovnih funkionalnosti", "[function]")
{
    //Normalni slucajevi upotrebe

    QVector<QString> names{"TinkiVinki,Lala,Po"};
    auto game = Game::getInstance(nullptr,names);

    SECTION("Da li isGameEnd vraca true ako je igra zavrsena")
    {
        // Arrange
        bool expectedGameEnd = true;

        // Act
        game->setIsGameEnd(true);

        //Assert
        REQUIRE(expectedGameEnd == game->isGameEnd());
    }

    SECTION("Da li indexOfPlayerWhoSaidCambio vraca indeks igraca koji je pozvao kambio")
    {
        // Arrange
        bool expectedIndex = 1;

        // Act
        game->setIndexOfPlayerWhoSaidCambio(1);

        //Assert
        REQUIRE(expectedIndex == game->indexOfPlayerWhoSaidCambio());
    }


    SECTION("Da li je klasa game sigleton")
    {
        // Act
        auto game1 = Game::getInstance(nullptr,names);
        auto game2 = Game::getInstance(nullptr,names);

        //Assert
        REQUIRE(game1 == game2);
    }

    SECTION("Da li resetRoundNumber postavlja broj runde na 0")
    {
        // Arrange
        int expectedRoundNumber = 0;

        // Act
        int roundNumber = game->resetRoundNumber();

        //Assert
        REQUIRE(expectedRoundNumber == roundNumber);
    }

    SECTION("Da li incrementRoundNumber vraca broj runde povecan za 1")
    {
        // Arrange
        bool expectedRoundNumber = game->roundNumber() + 1;

        // Act
        int roundNumberIncrement = game->incrementRoundNumber();

        //Assert
        REQUIRE(expectedRoundNumber == roundNumberIncrement);
    }

    SECTION("Da li je nextPlayer vraca indeks sledeceg igraca")
    {
        // Arrange
        int expectedNextPlayerIndex =  (game->indexOfCurrentPlayer() + 1) % names.size();

        // Act
        int nextPlayerIndex = game->nextPlayer();

        //Assert
        REQUIRE(nextPlayerIndex == expectedNextPlayerIndex);
    }

    //Specijalan slucaj

    SECTION("Da li nextPlayer nad poslednjim igracem vraca indeks igraca na indeks prvog")
    {
        // Arrange
        int expectedNextPlayerIndex =  game->indexOfCurrentPlayer();

        // Act
        int nextPlayerIndex = game->nextPlayer();
        nextPlayerIndex = game->nextPlayer();
        nextPlayerIndex = game->nextPlayer();

        //Assert
        REQUIRE(nextPlayerIndex == expectedNextPlayerIndex);
    }
}
