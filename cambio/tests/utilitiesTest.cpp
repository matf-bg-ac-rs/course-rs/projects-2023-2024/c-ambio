#include "catch.hpp"
#include<QVector>
#include<QString>
#include"../src/utilities.h"

TEST_CASE("Testiranje funckije flattenCardIndex instance klase Utilities za igraca sa rednim br 0, ako je broj igrača 4")
{
    // Arrange
    Utilities helper{0,4};

    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija ME")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(0,1,0);
        auto index1 = helper.flattenCardIndex(0,1,1);
        auto index2 = helper.flattenCardIndex(0,0,0);
        auto index3 = helper.flattenCardIndex(0,0,1);
        auto index4 = helper.flattenCardIndex(0,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija LEVO")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(1,0,0);
        auto index1 = helper.flattenCardIndex(1,1,0);
        auto index2 = helper.flattenCardIndex(1,0,1);
        auto index3 = helper.flattenCardIndex(1,1,1);
        auto index4 = helper.flattenCardIndex(1,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);

    }    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija GORE")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(2,0,1);
        auto index1 = helper.flattenCardIndex(2,0,0);
        auto index2 = helper.flattenCardIndex(2,1,1);
        auto index3 = helper.flattenCardIndex(2,1,0);
        auto index4 = helper.flattenCardIndex(2,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija DESNO")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(3,1,1);
        auto index1 = helper.flattenCardIndex(3,0,1);
        auto index2 = helper.flattenCardIndex(3,1,0);
        auto index3 = helper.flattenCardIndex(3,0,0);
        auto index4 = helper.flattenCardIndex(3,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
}
TEST_CASE("Testiranje funckije flattenCardIndex instance klase Utilities za igraca sa rednim br 1, ako je broj igrača 4")
{
    // Arrange
    Utilities helper{1,4};

    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija ME")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(1,1,0);
        auto index1 = helper.flattenCardIndex(1,1,1);
        auto index2 = helper.flattenCardIndex(1,0,0);
        auto index3 = helper.flattenCardIndex(1,0,1);
        auto index4 = helper.flattenCardIndex(1,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija LEVO")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(2,0,0);
        auto index1 = helper.flattenCardIndex(2,1,0);
        auto index2 = helper.flattenCardIndex(2,0,1);
        auto index3 = helper.flattenCardIndex(2,1,1);
        auto index4 = helper.flattenCardIndex(2,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija GORE")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(3,0,1);
        auto index1 = helper.flattenCardIndex(3,0,0);
        auto index2 = helper.flattenCardIndex(3,1,1);
        auto index3 = helper.flattenCardIndex(3,1,0);
        auto index4 = helper.flattenCardIndex(3,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);

    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija DESNO")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(0,1,1);
        auto index1 = helper.flattenCardIndex(0,0,1);
        auto index2 = helper.flattenCardIndex(0,1,0);
        auto index3 = helper.flattenCardIndex(0,0,0);
        auto index4 = helper.flattenCardIndex(0,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
}
TEST_CASE("Testiranje funckije flattenCardIndex instance klase Utilities za igraca sa rednim br 2, ako je broj igrača 4")
{
    // Arrange
    Utilities helper{2,4};

    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija ME")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(2,1,0);
        auto index1 = helper.flattenCardIndex(2,1,1);
        auto index2 = helper.flattenCardIndex(2,0,0);
        auto index3 = helper.flattenCardIndex(2,0,1);
        auto index4 = helper.flattenCardIndex(2,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija LEVO")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(3,0,0);
        auto index1 = helper.flattenCardIndex(3,1,0);
        auto index2 = helper.flattenCardIndex(3,0,1);
        auto index3 = helper.flattenCardIndex(3,1,1);
        auto index4 = helper.flattenCardIndex(3,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija GORE")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(0,0,1);
        auto index1 = helper.flattenCardIndex(0,0,0);
        auto index2 = helper.flattenCardIndex(0,1,1);
        auto index3 = helper.flattenCardIndex(0,1,0);
        auto index4 = helper.flattenCardIndex(0,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);

    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija DESNO")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(1,1,1);
        auto index1 = helper.flattenCardIndex(1,0,1);
        auto index2 = helper.flattenCardIndex(1,1,0);
        auto index3 = helper.flattenCardIndex(1,0,0);
        auto index4 = helper.flattenCardIndex(1,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
}
TEST_CASE("Testiranje funckije flattenCardIndex instance klase Utilities za igraca sa rednim br 3, ako je broj igrača 4")
{
    // Arrange
    Utilities helper{3,4};

    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija ME")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(3,1,0);
        auto index1 = helper.flattenCardIndex(3,1,1);
        auto index2 = helper.flattenCardIndex(3,0,0);
        auto index3 = helper.flattenCardIndex(3,0,1);
        auto index4 = helper.flattenCardIndex(3,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija LEVO")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(0,0,0);
        auto index1 = helper.flattenCardIndex(0,1,0);
        auto index2 = helper.flattenCardIndex(0,0,1);
        auto index3 = helper.flattenCardIndex(0,1,1);
        auto index4 = helper.flattenCardIndex(0,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija GORE")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(1,0,1);
        auto index1 = helper.flattenCardIndex(1,0,0);
        auto index2 = helper.flattenCardIndex(1,1,1);
        auto index3 = helper.flattenCardIndex(1,1,0);
        auto index4 = helper.flattenCardIndex(1,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);

    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija DESNO")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(2,1,1);
        auto index1 = helper.flattenCardIndex(2,0,1);
        auto index2 = helper.flattenCardIndex(2,1,0);
        auto index3 = helper.flattenCardIndex(2,0,0);
        auto index4 = helper.flattenCardIndex(2,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
}
TEST_CASE("Testiranje funckije flattenCardIndex instance klase Utilities za igraca sa rednim br 0, ako je broj igrača 3")
{
    // Arrange
    Utilities helper{0,3};

    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija ME")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(0,1,0);
        auto index1 = helper.flattenCardIndex(0,1,1);
        auto index2 = helper.flattenCardIndex(0,0,0);
        auto index3 = helper.flattenCardIndex(0,0,1);
        auto index4 = helper.flattenCardIndex(0,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija LEVO")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(1,0,0);
        auto index1 = helper.flattenCardIndex(1,1,0);
        auto index2 = helper.flattenCardIndex(1,0,1);
        auto index3 = helper.flattenCardIndex(1,1,1);
        auto index4 = helper.flattenCardIndex(1,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija GORE")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(2,0,1);
        auto index1 = helper.flattenCardIndex(2,0,0);
        auto index2 = helper.flattenCardIndex(2,1,1);
        auto index3 = helper.flattenCardIndex(2,1,0);
        auto index4 = helper.flattenCardIndex(2,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);

    }
}
TEST_CASE("Testiranje funckije flattenCardIndex instance klase Utilities za igraca sa rednim br 1, ako je broj igrača 3")
{
    // Arrange
    Utilities helper{1,3};

    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija ME")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(1,1,0);
        auto index1 = helper.flattenCardIndex(1,1,1);
        auto index2 = helper.flattenCardIndex(1,0,0);
        auto index3 = helper.flattenCardIndex(1,0,1);
        auto index4 = helper.flattenCardIndex(1,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija LEVO")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(2,0,0);
        auto index1 = helper.flattenCardIndex(2,1,0);
        auto index2 = helper.flattenCardIndex(2,0,1);
        auto index3 = helper.flattenCardIndex(2,1,1);
        auto index4 = helper.flattenCardIndex(2,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija GORE")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(0,0,1);
        auto index1 = helper.flattenCardIndex(0,0,0);
        auto index2 = helper.flattenCardIndex(0,1,1);
        auto index3 = helper.flattenCardIndex(0,1,0);
        auto index4 = helper.flattenCardIndex(0,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);

    }
}
TEST_CASE("Testiranje funckije flattenCardIndex instance klase Utilities za igraca sa rednim br 2, ako je broj igrača 3")
{
    // Arrange
    Utilities helper{2,3};

    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija ME")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(2,1,0);
        auto index1 = helper.flattenCardIndex(2,1,1);
        auto index2 = helper.flattenCardIndex(2,0,0);
        auto index3 = helper.flattenCardIndex(2,0,1);
        auto index4 = helper.flattenCardIndex(2,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija LEVO")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(0,0,0);
        auto index1 = helper.flattenCardIndex(0,1,0);
        auto index2 = helper.flattenCardIndex(0,0,1);
        auto index3 = helper.flattenCardIndex(0,1,1);
        auto index4 = helper.flattenCardIndex(0,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija GORE")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(1,0,1);
        auto index1 = helper.flattenCardIndex(1,0,0);
        auto index2 = helper.flattenCardIndex(1,1,1);
        auto index3 = helper.flattenCardIndex(1,1,0);
        auto index4 = helper.flattenCardIndex(1,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);

    }
}
TEST_CASE("Testiranje funckije flattenCardIndex instance klase Utilities za igraca sa rednim br 0, ako je broj igrača 2")
{
    // Arrange
    Utilities helper{0,2};

    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija ME")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(0,1,0);
        auto index1 = helper.flattenCardIndex(0,1,1);
        auto index2 = helper.flattenCardIndex(0,0,0);
        auto index3 = helper.flattenCardIndex(0,0,1);
        auto index4 = helper.flattenCardIndex(0,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija GORE")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(1,0,1);
        auto index1 = helper.flattenCardIndex(1,0,0);
        auto index2 = helper.flattenCardIndex(1,1,1);
        auto index3 = helper.flattenCardIndex(1,1,0);
        auto index4 = helper.flattenCardIndex(1,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
}

TEST_CASE("Testiranje funckije flattenCardIndex instance klase Utilities za igraca sa rednim br 1, ako je broj igrača 2")
{
    // Arrange
    Utilities helper{1,2};

    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija ME")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(1,1,0);
        auto index1 = helper.flattenCardIndex(1,1,1);
        auto index2 = helper.flattenCardIndex(1,0,0);
        auto index3 = helper.flattenCardIndex(1,0,1);
        auto index4 = helper.flattenCardIndex(1,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
    SECTION("Da li funkcija flattenCardIndex dobro mapira indekse karata iz matrice na GUI-u"
            " u odgovarajuce indekse iz vektora karata na GAME-u, provera za igraca cija je pozicija GORE")
    {
        //Arrange
        auto expected_index0 = 0;
        auto expected_index1 = 1;
        auto expected_index2 = 2;
        auto expected_index3 = 3;
        auto expected_index4 = 4;

        // Act
        auto index0 = helper.flattenCardIndex(0,0,1);
        auto index1 = helper.flattenCardIndex(0,0,0);
        auto index2 = helper.flattenCardIndex(0,1,1);
        auto index3 = helper.flattenCardIndex(0,1,0);
        auto index4 = helper.flattenCardIndex(0,0,2);

        //Assert
        REQUIRE(expected_index0 == index0);
        REQUIRE(expected_index1 == index1);
        REQUIRE(expected_index2 == index2);
        REQUIRE(expected_index3 == index3);
        REQUIRE(expected_index4 == index4);
    }
}
TEST_CASE("Testiranje funckije expandCardIndex instance klase Utilities za igraca sa rednim br 0, ako je broj igrača 4")
{
    // Arrange
    Utilities helper{0,4};

    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca ME")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,0);
        QPair<int, int> expected_pair1 = qMakePair(1,1);
        QPair<int, int> expected_pair2 = qMakePair(0,0);
        QPair<int, int> expected_pair3 = qMakePair(0,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(0,0);
        auto pair1 = helper.expandCardIndex(0,1);
        auto pair2 = helper.expandCardIndex(0,2);
        auto pair3 = helper.expandCardIndex(0,3);
        auto pair4 = helper.expandCardIndex(0,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca LEVO")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,0);
        QPair<int, int> expected_pair1 = qMakePair(1,0);
        QPair<int, int> expected_pair2 = qMakePair(0,1);
        QPair<int, int> expected_pair3 = qMakePair(1,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(1,0);
        auto pair1 = helper.expandCardIndex(1,1);
        auto pair2 = helper.expandCardIndex(1,2);
        auto pair3 = helper.expandCardIndex(1,3);
        auto pair4 = helper.expandCardIndex(1,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca GORE")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,1);
        QPair<int, int> expected_pair1 = qMakePair(0,0);
        QPair<int, int> expected_pair2 = qMakePair(1,1);
        QPair<int, int> expected_pair3 = qMakePair(1,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(2,0);
        auto pair1 = helper.expandCardIndex(2,1);
        auto pair2 = helper.expandCardIndex(2,2);
        auto pair3 = helper.expandCardIndex(2,3);
        auto pair4 = helper.expandCardIndex(2,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca DESNO")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,1);
        QPair<int, int> expected_pair1 = qMakePair(0,1);
        QPair<int, int> expected_pair2 = qMakePair(1,0);
        QPair<int, int> expected_pair3 = qMakePair(0,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(3,0);
        auto pair1 = helper.expandCardIndex(3,1);
        auto pair2 = helper.expandCardIndex(3,2);
        auto pair3 = helper.expandCardIndex(3,3);
        auto pair4 = helper.expandCardIndex(3,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
}
TEST_CASE("Testiranje funckije expandCardIndex instance klase Utilities za igraca sa rednim br 1, ako je broj igrača 4")
{
    // Arrange
    Utilities helper{1,4};

    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca ME")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,0);
        QPair<int, int> expected_pair1 = qMakePair(1,1);
        QPair<int, int> expected_pair2 = qMakePair(0,0);
        QPair<int, int> expected_pair3 = qMakePair(0,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(1,0);
        auto pair1 = helper.expandCardIndex(1,1);
        auto pair2 = helper.expandCardIndex(1,2);
        auto pair3 = helper.expandCardIndex(1,3);
        auto pair4 = helper.expandCardIndex(1,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca LEVO")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,0);
        QPair<int, int> expected_pair1 = qMakePair(1,0);
        QPair<int, int> expected_pair2 = qMakePair(0,1);
        QPair<int, int> expected_pair3 = qMakePair(1,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(2,0);
        auto pair1 = helper.expandCardIndex(2,1);
        auto pair2 = helper.expandCardIndex(2,2);
        auto pair3 = helper.expandCardIndex(2,3);
        auto pair4 = helper.expandCardIndex(2,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca GORE")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,1);
        QPair<int, int> expected_pair1 = qMakePair(0,0);
        QPair<int, int> expected_pair2 = qMakePair(1,1);
        QPair<int, int> expected_pair3 = qMakePair(1,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(3,0);
        auto pair1 = helper.expandCardIndex(3,1);
        auto pair2 = helper.expandCardIndex(3,2);
        auto pair3 = helper.expandCardIndex(3,3);
        auto pair4 = helper.expandCardIndex(3,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca DESNO")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,1);
        QPair<int, int> expected_pair1 = qMakePair(0,1);
        QPair<int, int> expected_pair2 = qMakePair(1,0);
        QPair<int, int> expected_pair3 = qMakePair(0,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(0,0);
        auto pair1 = helper.expandCardIndex(0,1);
        auto pair2 = helper.expandCardIndex(0,2);
        auto pair3 = helper.expandCardIndex(0,3);
        auto pair4 = helper.expandCardIndex(0,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
}
TEST_CASE("Testiranje funckije expandCardIndex instance klase Utilities za igraca sa rednim br 2, ako je broj igrača 4")
{
    // Arrange
    Utilities helper{2,4};

    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca ME")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,0);
        QPair<int, int> expected_pair1 = qMakePair(1,1);
        QPair<int, int> expected_pair2 = qMakePair(0,0);
        QPair<int, int> expected_pair3 = qMakePair(0,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(2,0);
        auto pair1 = helper.expandCardIndex(2,1);
        auto pair2 = helper.expandCardIndex(2,2);
        auto pair3 = helper.expandCardIndex(2,3);
        auto pair4 = helper.expandCardIndex(2,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca LEVO")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,0);
        QPair<int, int> expected_pair1 = qMakePair(1,0);
        QPair<int, int> expected_pair2 = qMakePair(0,1);
        QPair<int, int> expected_pair3 = qMakePair(1,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(3,0);
        auto pair1 = helper.expandCardIndex(3,1);
        auto pair2 = helper.expandCardIndex(3,2);
        auto pair3 = helper.expandCardIndex(3,3);
        auto pair4 = helper.expandCardIndex(3,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca GORE")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,1);
        QPair<int, int> expected_pair1 = qMakePair(0,0);
        QPair<int, int> expected_pair2 = qMakePair(1,1);
        QPair<int, int> expected_pair3 = qMakePair(1,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(0,0);
        auto pair1 = helper.expandCardIndex(0,1);
        auto pair2 = helper.expandCardIndex(0,2);
        auto pair3 = helper.expandCardIndex(0,3);
        auto pair4 = helper.expandCardIndex(0,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca DESNO")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,1);
        QPair<int, int> expected_pair1 = qMakePair(0,1);
        QPair<int, int> expected_pair2 = qMakePair(1,0);
        QPair<int, int> expected_pair3 = qMakePair(0,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(1,0);
        auto pair1 = helper.expandCardIndex(1,1);
        auto pair2 = helper.expandCardIndex(1,2);
        auto pair3 = helper.expandCardIndex(1,3);
        auto pair4 = helper.expandCardIndex(1,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
}
TEST_CASE("Testiranje funckije expandCardIndex instance klase Utilities za igraca sa rednim br 3, ako je broj igrača 4")
{
    // Arrange
    Utilities helper{3,4};

    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca ME")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,0);
        QPair<int, int> expected_pair1 = qMakePair(1,1);
        QPair<int, int> expected_pair2 = qMakePair(0,0);
        QPair<int, int> expected_pair3 = qMakePair(0,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(3,0);
        auto pair1 = helper.expandCardIndex(3,1);
        auto pair2 = helper.expandCardIndex(3,2);
        auto pair3 = helper.expandCardIndex(3,3);
        auto pair4 = helper.expandCardIndex(3,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca LEVO")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,0);
        QPair<int, int> expected_pair1 = qMakePair(1,0);
        QPair<int, int> expected_pair2 = qMakePair(0,1);
        QPair<int, int> expected_pair3 = qMakePair(1,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(0,0);
        auto pair1 = helper.expandCardIndex(0,1);
        auto pair2 = helper.expandCardIndex(0,2);
        auto pair3 = helper.expandCardIndex(0,3);
        auto pair4 = helper.expandCardIndex(0,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca GORE")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,1);
        QPair<int, int> expected_pair1 = qMakePair(0,0);
        QPair<int, int> expected_pair2 = qMakePair(1,1);
        QPair<int, int> expected_pair3 = qMakePair(1,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(1,0);
        auto pair1 = helper.expandCardIndex(1,1);
        auto pair2 = helper.expandCardIndex(1,2);
        auto pair3 = helper.expandCardIndex(1,3);
        auto pair4 = helper.expandCardIndex(1,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca DESNO")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,1);
        QPair<int, int> expected_pair1 = qMakePair(0,1);
        QPair<int, int> expected_pair2 = qMakePair(1,0);
        QPair<int, int> expected_pair3 = qMakePair(0,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(2,0);
        auto pair1 = helper.expandCardIndex(2,1);
        auto pair2 = helper.expandCardIndex(2,2);
        auto pair3 = helper.expandCardIndex(2,3);
        auto pair4 = helper.expandCardIndex(2,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
}
TEST_CASE("Testiranje funckije expandCardIndex instance klase Utilities za igraca sa rednim br 0, ako je broj igrača 3")
{
    // Arrange
    Utilities helper{0,3};

    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca ME")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,0);
        QPair<int, int> expected_pair1 = qMakePair(1,1);
        QPair<int, int> expected_pair2 = qMakePair(0,0);
        QPair<int, int> expected_pair3 = qMakePair(0,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(0,0);
        auto pair1 = helper.expandCardIndex(0,1);
        auto pair2 = helper.expandCardIndex(0,2);
        auto pair3 = helper.expandCardIndex(0,3);
        auto pair4 = helper.expandCardIndex(0,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca LEVO")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,0);
        QPair<int, int> expected_pair1 = qMakePair(1,0);
        QPair<int, int> expected_pair2 = qMakePair(0,1);
        QPair<int, int> expected_pair3 = qMakePair(1,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(1,0);
        auto pair1 = helper.expandCardIndex(1,1);
        auto pair2 = helper.expandCardIndex(1,2);
        auto pair3 = helper.expandCardIndex(1,3);
        auto pair4 = helper.expandCardIndex(1,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca GORE")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,1);
        QPair<int, int> expected_pair1 = qMakePair(0,0);
        QPair<int, int> expected_pair2 = qMakePair(1,1);
        QPair<int, int> expected_pair3 = qMakePair(1,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(2,0);
        auto pair1 = helper.expandCardIndex(2,1);
        auto pair2 = helper.expandCardIndex(2,2);
        auto pair3 = helper.expandCardIndex(2,3);
        auto pair4 = helper.expandCardIndex(2,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
}
TEST_CASE("Testiranje funckije expandCardIndex instance klase Utilities za igraca sa rednim br 1, ako je broj igrača 3")
{
    // Arrange
    Utilities helper{1,3};

    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca ME")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,0);
        QPair<int, int> expected_pair1 = qMakePair(1,1);
        QPair<int, int> expected_pair2 = qMakePair(0,0);
        QPair<int, int> expected_pair3 = qMakePair(0,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(1,0);
        auto pair1 = helper.expandCardIndex(1,1);
        auto pair2 = helper.expandCardIndex(1,2);
        auto pair3 = helper.expandCardIndex(1,3);
        auto pair4 = helper.expandCardIndex(1,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca LEVO")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,0);
        QPair<int, int> expected_pair1 = qMakePair(1,0);
        QPair<int, int> expected_pair2 = qMakePair(0,1);
        QPair<int, int> expected_pair3 = qMakePair(1,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(2,0);
        auto pair1 = helper.expandCardIndex(2,1);
        auto pair2 = helper.expandCardIndex(2,2);
        auto pair3 = helper.expandCardIndex(2,3);
        auto pair4 = helper.expandCardIndex(2,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca GORE")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,1);
        QPair<int, int> expected_pair1 = qMakePair(0,0);
        QPair<int, int> expected_pair2 = qMakePair(1,1);
        QPair<int, int> expected_pair3 = qMakePair(1,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(0,0);
        auto pair1 = helper.expandCardIndex(0,1);
        auto pair2 = helper.expandCardIndex(0,2);
        auto pair3 = helper.expandCardIndex(0,3);
        auto pair4 = helper.expandCardIndex(0,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
}
TEST_CASE("Testiranje funckije expandCardIndex instance klase Utilities za igraca sa rednim br 2, ako je broj igrača 3")
{
    // Arrange
    Utilities helper{2,3};

    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca ME")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,0);
        QPair<int, int> expected_pair1 = qMakePair(1,1);
        QPair<int, int> expected_pair2 = qMakePair(0,0);
        QPair<int, int> expected_pair3 = qMakePair(0,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(2,0);
        auto pair1 = helper.expandCardIndex(2,1);
        auto pair2 = helper.expandCardIndex(2,2);
        auto pair3 = helper.expandCardIndex(2,3);
        auto pair4 = helper.expandCardIndex(2,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca LEVO")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,0);
        QPair<int, int> expected_pair1 = qMakePair(1,0);
        QPair<int, int> expected_pair2 = qMakePair(0,1);
        QPair<int, int> expected_pair3 = qMakePair(1,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(0,0);
        auto pair1 = helper.expandCardIndex(0,1);
        auto pair2 = helper.expandCardIndex(0,2);
        auto pair3 = helper.expandCardIndex(0,3);
        auto pair4 = helper.expandCardIndex(0,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca GORE")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,1);
        QPair<int, int> expected_pair1 = qMakePair(0,0);
        QPair<int, int> expected_pair2 = qMakePair(1,1);
        QPair<int, int> expected_pair3 = qMakePair(1,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(1,0);
        auto pair1 = helper.expandCardIndex(1,1);
        auto pair2 = helper.expandCardIndex(1,2);
        auto pair3 = helper.expandCardIndex(1,3);
        auto pair4 = helper.expandCardIndex(1,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
}
TEST_CASE("Testiranje funckije expandCardIndex instance klase Utilities za igraca sa rednim br 0, ako je broj igrača 2")
{
    // Arrange
    Utilities helper{0,2};

    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca ME")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,0);
        QPair<int, int> expected_pair1 = qMakePair(1,1);
        QPair<int, int> expected_pair2 = qMakePair(0,0);
        QPair<int, int> expected_pair3 = qMakePair(0,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(0,0);
        auto pair1 = helper.expandCardIndex(0,1);
        auto pair2 = helper.expandCardIndex(0,2);
        auto pair3 = helper.expandCardIndex(0,3);
        auto pair4 = helper.expandCardIndex(0,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca GORE")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,1);
        QPair<int, int> expected_pair1 = qMakePair(0,0);
        QPair<int, int> expected_pair2 = qMakePair(1,1);
        QPair<int, int> expected_pair3 = qMakePair(1,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(1,0);
        auto pair1 = helper.expandCardIndex(1,1);
        auto pair2 = helper.expandCardIndex(1,2);
        auto pair3 = helper.expandCardIndex(1,3);
        auto pair4 = helper.expandCardIndex(1,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
}
TEST_CASE("Testiranje funckije expandCardIndex instance klase Utilities za igraca sa rednim br 1, ako je broj igrača 2")
{
    // Arrange
    Utilities helper{1,2};

    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca ME")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(1,0);
        QPair<int, int> expected_pair1 = qMakePair(1,1);
        QPair<int, int> expected_pair2 = qMakePair(0,0);
        QPair<int, int> expected_pair3 = qMakePair(0,1);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(1,0);
        auto pair1 = helper.expandCardIndex(1,1);
        auto pair2 = helper.expandCardIndex(1,2);
        auto pair3 = helper.expandCardIndex(1,3);
        auto pair4 = helper.expandCardIndex(1,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
    SECTION("Da li funkcija expandCardIndex dobro mapira indekse karata iz vektora karata na Game-u"
            " u odgovarajuce indekse iz matrice karata na Gui-u, ako je poslata karta igraca GORE")
    {
        //Arrange
        QPair<int, int> expected_pair0 = qMakePair(0,1);
        QPair<int, int> expected_pair1 = qMakePair(0,0);
        QPair<int, int> expected_pair2 = qMakePair(1,1);
        QPair<int, int> expected_pair3 = qMakePair(1,0);
        QPair<int, int> expected_pair4 = qMakePair(0,2);

        // Act
        auto pair0 = helper.expandCardIndex(0,0);
        auto pair1 = helper.expandCardIndex(0,1);
        auto pair2 = helper.expandCardIndex(0,2);
        auto pair3 = helper.expandCardIndex(0,3);
        auto pair4 = helper.expandCardIndex(0,4);

        //Assert
        REQUIRE(expected_pair0 == pair0);
        REQUIRE(expected_pair1 == pair1);
        REQUIRE(expected_pair2 == pair2);
        REQUIRE(expected_pair3 == pair3);
        REQUIRE(expected_pair4 == pair4);
    }
}
