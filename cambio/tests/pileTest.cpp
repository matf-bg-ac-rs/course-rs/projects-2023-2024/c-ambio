#include "catch.hpp"
#include "../src/game/pile.h"
#include<QVector>
#include<QString>

class Card;

TEST_CASE("testiranje klase Pile i osnovnih funkionalnosti", "[function]")
{
    //Normalan slucaj upotrebe

    SECTION("Da li pri konstrukciji imamo prazan pile")
    {
        // Arrange
        size_t expected_pileSize = 0;
        bool expected_isEmpty = true;

        // Act

        Pile pile;

        //Assert
        REQUIRE(expected_pileSize == pile.pileSize());
        REQUIRE(expected_isEmpty == pile.isEmpty());

    }
    SECTION("Da li se pri dodavanju jedne karte na Pile povecava velicina pile-a za jedan")
    {
        // Arrange
        Card *karta = Card::createCard(5);
        Pile pile;
        size_t expected_pileSize = pile.pileSize() + 1;


        // Act
        pile.addCardToPile(karta);


        //Assert
        REQUIRE(expected_pileSize == pile.pileSize());

    }
    SECTION("Da li je nakon dodavanja karte na pile, karta na vrhu pile-a jednaka poslednje dodatoj karti")
    {
        // Arrange
        Card *expected_card = Card::createCard(5);
        Pile pile;


        // Act
        pile.addCardToPile(expected_card);


        //Assert
        REQUIRE(expected_card == pile.peekFirstCard());

    }
    SECTION("Da li je nakon getFirstCard skinuta karta sa vrha pile-a")
    {
        // Arrange
        Card *card = Card::createCard(5);
        Pile pile;
        pile.addCardToPile(card);
        Card *expected_card = pile.peekFirstCard();


        // Act
        Card* cardOnTop = pile.getFirstCard();


        //Assert
        REQUIRE(expected_card == cardOnTop);

    }
    SECTION("Da li nakon get_top_kard smanjuje velicina pile-a")
    {
        // Arrange
        Card *card = Card::createCard(5);
        Pile pile;
        pile.addCardToPile(card);
        size_t expected_sizeOfPile = pile.pileSize() - 1;



        // Act
        Card* cardOnTop = pile.getFirstCard();
        delete cardOnTop;


        //Assert
        REQUIRE(expected_sizeOfPile == pile.pileSize());

    }
    // novi test!
    SECTION("Da li nakon peek velicina pile-a ostaje nepromenjena")
    {
        // Arrange
        Card *card = Card::createCard(5);
        Pile pile;
        pile.addCardToPile(card);
        size_t expected_sizeOfPile = pile.pileSize();



        // Act
        Card* cardOnTop = pile.peekFirstCard();

        //Assert
        REQUIRE(expected_sizeOfPile == pile.pileSize());

    }
}
