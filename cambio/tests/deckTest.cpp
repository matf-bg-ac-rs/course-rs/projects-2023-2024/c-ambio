#include "catch.hpp"
#include "../src/game/deck.h"
#include<QVector>
#include<QString>

class Card;

TEST_CASE("testiranje klase Deck i osnovnih funkionalnosti", "[function]")
{
    //Normalan slucaj upotrebe

    SECTION("Da li pri konstrukciji imamo 108 karata")
    {
        // Arrange
        size_t expectedDeckSize = 108;

        // Act
        Deck deck;

        //Assert
        REQUIRE(expectedDeckSize == deck.deckSize());
    }

    SECTION("Da li se pri izvlacenju karte sa deck-a velicina deck-a smanjuje za jedan")
    {
        // Arrange
        Deck deck;
        size_t expectedDeckSize = deck.deckSize() - 1;


        // Act
        deck.getFirstCard();

        //Assert
        REQUIRE(expectedDeckSize == deck.deckSize());
    }

    SECTION("Da li je nakon getFirstCard skinuta karta sa vrha deck-a")
    {
        // Arrange
        Deck deck;
        Card *expectedCard = deck.peekFirstCard();

        // Act
        Card* cardOnTop = deck.getFirstCard();

        //Assert
        REQUIRE(expectedCard == cardOnTop);

//        delete expectedCard;
//        delete cardOnTop;
    }

    SECTION("Da li je nakon dealHand-a broj karata u ruci 4")
    {
        // Arrange
        Deck deck;
        size_t expectedHandSize = 4;

        // Act
        QVector<Card*> hand = deck.dealHand();

        //Assert
        REQUIRE(expectedHandSize == hand.size());
    }

    SECTION("Da li je nakon dealHand-a smanjuje broj karata iz spila za 4")
    {
        // Arrange
        Deck deck;
        size_t expectedDeckSize = deck.deckSize() - 4;

        // Act
        QVector<Card*> hand = deck.dealHand();

        //Assert
        REQUIRE(expectedDeckSize == deck.deckSize());

        for (int i = 0; i<hand.size();i++)
        {
            delete hand[i];
        }
    }
    SECTION("Da li je nakon peek velicina deck-a ostaje nepromenjena")
    {
        // Arrange
        Deck deck;
        size_t expectedDeckSize = deck.deckSize();

        // Act
        Card* card = deck.peekFirstCard();

        //Assert
        REQUIRE(expectedDeckSize == deck.deckSize());
    }
    SECTION("Da li getFirsCard ispaljuje exception ako je deck prazan")
    {
        // Arrange
        Deck deck;
        auto size = deck.deckSize();
        for(int i = 0; i < size; i++)
        {
            deck.getFirstCard();
        }


        //Act + Assert
        REQUIRE_THROWS(deck.getFirstCard());
    }
    SECTION("Da li peekFirstCard ispaljuje exception ako je deck prazan")
    {
        // Arrange
        Deck deck;
        auto size = deck.deckSize();
        for(int i = 0; i < size; i++)
        {
            deck.getFirstCard();
        };


        //Act + Assert
        REQUIRE_THROWS(deck.peekFirstCard());
    }
    SECTION("Da li nakon refill deck-a ima odgovarajuc broj karata")
    {
        // Arrange
        Deck deck;
        Pile pile;
        auto size = deck.deckSize();
        for(int i = 0; i < size; i++)
        {
            pile.addCardToPile(deck.getFirstCard());
        };
        auto expected_size = size - 1;


        //Act
        deck.refill(&pile);

        //Assert
        REQUIRE(expected_size == deck.deckSize());
    }
}
