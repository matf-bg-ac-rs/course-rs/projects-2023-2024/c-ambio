#include "catch.hpp"
#include<QVector>
#include<QString>
#include"../src/packetparser.h"

TEST_CASE("testiranje klase packetParser, provera tipova paketa", "[function]")
{

    ReadyPacket ready{true};
    auto readyData = ready.toSend();

    EndGame endGame{};
    auto endGameData = endGame.toSend();

    ErrorPacket error{"Neka porukica"};
    auto errorData = error.toSend();

    FlyIntoRequest flyIntoReq{5,2,2};
    auto flyIntReqData = flyIntoReq.toSend();

    auto gameStateData = GameStatePacket{MOVE_TYPE::DECK, 5, true, 0, 2, 1}.toSend();

    auto gameStateResData =  GameStateResponse{MOVE_TYPE::PILE, 2, 1}.toSend();

    auto loginReqData = LoginRequestPacket{"Peroljub",2}.toSend();

    QVector<QPair<QString,int>> igraci{};
    auto loginResData = LoginResponsePacket{1,true,igraci}.toSend();


    auto startGameReqData = StartGameRequest{false,"Peroljub",2}.toSend();


    QVector<QString>imena{};
    QVector<int> avatari{};
    auto startGameResData = StartGameResponse{imena,avatari,"reg5","magic7","reg2"}.toSend();

    //Normalni slucajevi upotrebe

    SECTION("Da li paket parser za paket readyPacket vraca dobar tip")
    {
        //Act
        Packet *readyPacket = PacketParser::parsePacket(readyData);

        //Assert
        REQUIRE(readyPacket->type() == "ready");
        delete readyPacket;
    }

    SECTION("Da li paket parser za paket endPacket vraca dobar tip")
    {
        //Act
        Packet *endPacket = PacketParser::parsePacket(endGameData);

        //Assert
        REQUIRE(endPacket->type() == "end game");
        delete endPacket;
    }

    SECTION("Da li paket parser za paket errorPacket vraca dobar tip")
    {
        //Act
        Packet *errorPacket = PacketParser::parsePacket(errorData);

        //Assert
        REQUIRE(errorPacket->type() == "error");
        delete errorPacket;
    }

    SECTION("Da li paket parser za paket flyRegPacket vraca dobar tip")
    {
        //Act
        Packet *flyReqPacket = PacketParser::parsePacket(flyIntReqData);

        //Assert
        REQUIRE(flyReqPacket->type() == "fly into");
        delete flyReqPacket;
    }

    SECTION("Da li paket parser za paket gameStatePacket vraca dobar tip")
    {
        //Act
        Packet *gameStatePacket = PacketParser::parsePacket(gameStateData);

        //Assert
        REQUIRE(gameStatePacket->type() == "game state packet");
        delete gameStatePacket;
    }

    SECTION("Da li paket parser za paket gameStateResPacket vraca dobar tip")
    {
        //Act
        Packet *gameStateResPacket = PacketParser::parsePacket(gameStateResData);

        //Assert
        REQUIRE(gameStateResPacket->type() == "game state response");
        delete gameStateResPacket;
    }

    SECTION("Da li paket parser za paket LoginRequestPacket vraca dobar tip")
    {
        //Act
        Packet *LoginRequestPacket = PacketParser::parsePacket(loginReqData);

        //Assert
        REQUIRE(LoginRequestPacket->type() == "login request");
        delete LoginRequestPacket;
    }

    SECTION("Da li paket parser za paket LoginResPacket vraca dobar tip")
    {
        //Act
        Packet *LoginResPacket = PacketParser::parsePacket(loginResData);

        //Assert
        REQUIRE(LoginResPacket->type() == "login response");
        delete LoginResPacket;
    }

    SECTION("Da li paket parser za paket StartGameReqPacket vraca dobar tip")
    {
        //Act
        Packet *StartGameReqPacket = PacketParser::parsePacket(startGameReqData);

        //Assert
        REQUIRE(StartGameReqPacket->type() == "start game request");
        delete StartGameReqPacket;
    }

    SECTION("Da li paket parser za paket StartGameResPacket vraca dobar tip")
    {
        //Act
        Packet *StartGameResPacket = PacketParser::parsePacket(startGameResData);

        //Assert
        REQUIRE(StartGameResPacket->type() == "start game response");
        delete StartGameResPacket;
    }
}

TEST_CASE("Testiranje klase PacketParser, parsiranje flyIntoRequest paketa", "[function]")
{
    //Arrange
    auto expectedRoundNum =5;
    auto expectedIndexOfFlyInto = 2;
    auto expectedCardIndex = 2;
    FlyIntoRequest flReq{expectedRoundNum, expectedIndexOfFlyInto, expectedCardIndex};

    SECTION("Da li FlyIntoRequest vraca dobar broj runde")
    {
        //Act
        FlyIntoRequest* flyIntoRequestPacket = dynamic_cast<FlyIntoRequest*>(PacketParser::parsePacket(flReq.toSend()));

        //Assert
        REQUIRE(expectedRoundNum == flyIntoRequestPacket->roundNum() );
        delete flyIntoRequestPacket;
    }

    SECTION("Da li FlyIntoRequest vraca dobar indeks igraca sa cijom kartom je uleteno")
    {
        //Act
        FlyIntoRequest* flyIntoRequestPacket = dynamic_cast<FlyIntoRequest*>(PacketParser::parsePacket(flReq.toSend()));

        //Assert
        REQUIRE(expectedIndexOfFlyInto == flyIntoRequestPacket->flyIntoPlayer());
        delete flyIntoRequestPacket;
    }

    SECTION("Da li FlyIntoRequest vraca dobar indeks karte sa kojom je uleteno")
    {
        //Act
        FlyIntoRequest* flyIntoRequestPacket = dynamic_cast<FlyIntoRequest*>(PacketParser::parsePacket(flReq.toSend()));

        //Assert
        REQUIRE(expectedCardIndex == flyIntoRequestPacket->flyIntoIndexOfCard() );
        delete flyIntoRequestPacket;
    }
}

TEST_CASE("Testiranje klase PacketParser, parsiranje flyIntoResponse paketa", "[function]")
{
    //Arrange
    auto expected_roundNum = 5;
    auto expected_success = false;
    QString expected_card = "reg:5";
    auto expected_cardOwner = 0;
    auto expected_cardIndex = 2;
    auto expected_playerIndex = 2;
    auto expected_indexOfPenaltyCard = 1;

    FlyIntoResponse flyInRes{expected_roundNum,expected_success,expected_card,
                             expected_cardOwner,expected_playerIndex,expected_cardIndex, expected_indexOfPenaltyCard};


    //Act
    FlyIntoResponse* flyIntoResponsePacket = dynamic_cast<FlyIntoResponse*>(PacketParser::parsePacket(flyInRes.toSend()));

    SECTION("Provera parsiranja roundNumber-a")
    {
        //Assert
        REQUIRE(expected_roundNum == flyIntoResponsePacket->roundNum());
    }

    SECTION("Provera parsiranja succsess")
    {
        //Assert
        REQUIRE(expected_success == flyIntoResponsePacket->successFlyInto());

    }

    SECTION("Provera parsiranja card")
    {
        //Assert
        REQUIRE(expected_card == flyIntoResponsePacket->card());
    }
    SECTION("Provera parsiranja cardOwner")
    {
        //Assert
        REQUIRE(expected_cardOwner == flyIntoResponsePacket->cardOwner());
    }
    SECTION("Provera parsiranja cardIndex")
    {
        //Assert
        REQUIRE(expected_cardIndex == flyIntoResponsePacket->cardIndex());
    }
    SECTION("Provera parsiranja playerIndex")
    {
        //Assert
        REQUIRE(expected_playerIndex == flyIntoResponsePacket->playerIndex());
    }
    SECTION("Provera parsiranja IndexOfPenaltyCard")
    {
        //Assert
        REQUIRE(expected_indexOfPenaltyCard == flyIntoResponsePacket->indexOfPenaltyCard());
    }



}

TEST_CASE("Testiranje klase PacketParser, parsiranje gameState paketa", "[function]")
{
    //Arrange
    auto expected_subtype = MOVE_TYPE::DECK;
    auto expected_round_num = 5;
    auto expected_player_cambio = false;
    auto expected_player_from = -1;
    auto expected_index_card_from_player = -1;
    auto expected_player_to = -1;
    auto expected_index_card_to_player = -1;

    GameStatePacket packet{expected_subtype, expected_round_num};

    //Act
    GameStatePacket* gameState = dynamic_cast<GameStatePacket*>(PacketParser::parsePacket(packet.toSend()));

    SECTION("Provera parsiranja subtype-a")
    {
        //Assert
        REQUIRE(expected_subtype == gameState->subtype());
    }
    SECTION("Provera parsiranja round num-a")
    {
        //Assert
        REQUIRE(expected_round_num == gameState->roundNum());
    }
    SECTION("Provera parsiranja playerCambio")
    {
        //Assert
        REQUIRE(expected_player_cambio == gameState->playerCambio());
    }
    SECTION("Provera parsiranja player from")
    {
        //Assert
        REQUIRE(expected_player_from== gameState->playerFrom());
    }
    SECTION("Provera parsiranja player to")
    {
        //Assert
        REQUIRE(expected_player_to== gameState->playerTo());
    }
    SECTION("Provera parsiranja index card from")
    {
        //Assert
        REQUIRE(expected_index_card_from_player== gameState->indexCardFromPlayer());
    }
    SECTION("Provera parsiranja index card to")
    {
        //Assert
        REQUIRE(expected_index_card_to_player== gameState->indexCardToPlayer());
    }
}
TEST_CASE("Testiranje klase PacketParser, parsiranje gameStateResponse paketa", "[function]")
{
    //Arrange
    auto expected_subtype = MOVE_TYPE::PILE;
    auto expected_round_num = 2;
    auto expected_index_of_current_player = 0;
    auto expected_player_cambio = -1;
    auto expected_player_from = -1;
    auto expected_index_card_from_player = -1;
    auto expected_player_to = -1;
    auto expected_index_card_to_player = -1;


    GameStateResponse packet{expected_subtype, expected_round_num, expected_index_of_current_player, expected_player_cambio};

    //Act
    GameStateResponse* gameState = dynamic_cast<GameStateResponse*>(PacketParser::parsePacket(packet.toSend()));

    SECTION("Provera parsiranja subtype-a")
    {
        //Assert
        REQUIRE(expected_subtype == gameState->subtype());
    }
    SECTION("Provera parsiranja round num-a")
    {
        //Assert
        REQUIRE(expected_player_cambio == gameState->playerCambio());
    }
    SECTION("Provera parsiranja index-a trenutnog igraca")
    {
        //Assert
        REQUIRE(expected_index_of_current_player == gameState->indexOfCurrentPlayer());
    }
    SECTION("Provera parsiranja player from")
    {
        //Assert
        REQUIRE(expected_player_from== gameState->playerFrom());
    }
    SECTION("Provera parsiranja player to")
    {
        //Assert
        REQUIRE(expected_player_to== gameState->playerTo());
    }
    SECTION("Provera parsiranja index card from")
    {
        //Assert
        REQUIRE(expected_index_card_from_player== gameState->indexCardFromPlayer());
    }
    SECTION("Provera parsiranja index card to")
    {
        //Assert
        REQUIRE(expected_index_card_to_player== gameState->indexCardToPlayer());
    }
}

TEST_CASE("Testiranje klase PacketParser, parsiranje StartGameRespons paketa", "[function]")
{
    //Arrange
    QVector<QString> expected_names_of_players = {};
    QVector<int> expected_avatars_of_players = {};
    QVector<QString> expected_first_two_cards;
    QString expected_pile_top = "reg:blue:6";
    QString firstCard = "magic:detective:8";
    QString secondCard = "reg:pink:5";
    expected_first_two_cards.push_back(firstCard);
    expected_first_two_cards.push_back(secondCard);
    StartGameResponse packet{expected_names_of_players, expected_avatars_of_players, firstCard, secondCard , expected_pile_top};

    //Act
    StartGameResponse* startRes = dynamic_cast<StartGameResponse*>(PacketParser::parsePacket(packet.toSend()));

    SECTION("Provera parsiranja imena igraca")
    {
        //Assert
        REQUIRE(expected_names_of_players == startRes->namesOfPlayers());
    }
    SECTION("Provera parsiranja avatara igraca")
    {
        //Assert
        REQUIRE(expected_avatars_of_players == startRes->avatarsOfPlayers());
    }
    SECTION("Provera parsiranja prvih karata igraca")
    {
        //Assert
        REQUIRE(expected_first_two_cards == startRes->firstTwoCards());
    }
    SECTION("Provera parsiranja karte na vrhu pile-a")
    {
        //Assert
        REQUIRE(expected_pile_top == startRes->pileTop());
    }
    startRes->deleteLater();
}

TEST_CASE("Testiranje klase PacketParser, parsiranje loginRequst paketa", "[function]")
{
    //Arrange
    QString expected_name = "Luna";
    int expected_avatar = 5;

    LoginRequestPacket login{expected_name, expected_avatar};

    //Act
    LoginRequestPacket* loginRequest = dynamic_cast<LoginRequestPacket*>(PacketParser::parsePacket(login.toSend()));

    SECTION("Provera parsiranja imena-a")
    {
        //Assert
        REQUIRE(expected_name == loginRequest->name());
    }

    SECTION("Provera parsiranja avatara")
    {
        //Assert
        REQUIRE(expected_avatar == loginRequest->avatar());

    }
    loginRequest->deleteLater();
}
TEST_CASE("Testiranje klase PacketParser, parsiranje loginResponse paketa", "[function]")
{
    //Arrange
    int expected_index_in_game = 0;
    bool expected_is_Host = true;
    QVector<QPair<QString, int>> expected_connected_players {};


    LoginResponsePacket login{expected_index_in_game, expected_is_Host, expected_connected_players};

    //Act
    LoginResponsePacket* loginResponse = dynamic_cast<LoginResponsePacket*>(PacketParser::parsePacket(login.toSend()));

    SECTION("Provera parsiranja indeksa u igri-a")
    {
        //Assert
        REQUIRE(expected_index_in_game == loginResponse->indexInGame());
    }

    SECTION("Provera parsiranja da li je igrac domacin")
    {
        //Assert
        REQUIRE(expected_is_Host == loginResponse->isHost());
    }

    SECTION("Provera parsiranja konektovanih igraca")
    {
        //Assert
        REQUIRE(expected_connected_players == loginResponse->alreadyConnectedPlayers());
    }
    loginResponse->deleteLater();
}

TEST_CASE("Testiranje klase PacketParser, parsiranje error paketa", "[function]")
{
    //Arrange
    QString expected_message = "Uspesno testiranje";

    ErrorPacket error{expected_message};

    //Act
    ErrorPacket* errorPacket = dynamic_cast<ErrorPacket*>(PacketParser::parsePacket(error.toSend()));

    SECTION("Provera parsiranja poruke za gresku")
    {
        //Assert
        REQUIRE(expected_message == errorPacket->msg());
    }
    errorPacket->deleteLater();
}
