#ifndef STARTGAMEREQUEST_H
#define STARTGAMEREQUEST_H

#include "packet.h"

class StartGameRequest : public Packet
{
    Q_OBJECT
public:
    explicit StartGameRequest(const bool isHost, const QString &name, const int indexInGame);

    ~StartGameRequest() = default;

    QString name() const;
    bool isHost() const;
    QByteArray toSend() const override;

    int indexInGame() const;

private:
    const QString m_name;
    const bool m_isHost;
    const int m_indexInGame;
};

#endif // STARTGAMEREQUEST_H
