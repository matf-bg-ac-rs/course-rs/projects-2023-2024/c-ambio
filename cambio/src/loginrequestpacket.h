#ifndef LOGINREQUESTPACKET_H
#define LOGINREQUESTPACKET_H

#include "packet.h"

class LoginRequestPacket : public Packet
{
    Q_OBJECT
public:
    explicit LoginRequestPacket(const QString &name, const int avatar);

    ~LoginRequestPacket() = default;

    QString name() const;
    int avatar() const;
    QByteArray toSend() const override;

private:
    QString m_name;
    int m_avatar;
};

#endif // LOGINREQUESTPACKET_H
