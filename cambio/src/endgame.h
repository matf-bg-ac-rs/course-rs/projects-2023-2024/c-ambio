#ifndef ENDGAME_H
#define ENDGAME_H

#include "packet.h"

class EndGame : public Packet
{
    Q_OBJECT

public:
    EndGame(const QVector<QPair<QString, int>> &result, const QMap<int, QVector<QString>> &playersCards, const int winnersIndex);
    EndGame();

    QVector<QPair<QString, int>> result() const;

    QByteArray toSend() const override;


    QMap<int, QVector<QString> > playersCards() const;

    int winnersIndex() const;

private:
    const QVector<QPair<QString, int>> m_result;
    const QMap<int, QVector<QString>> m_playersCards;
    const int m_winnersIndex;
};

#endif // ENDGAME_H
