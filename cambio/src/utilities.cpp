#include "utilities.h"

Utilities::Utilities()
{
}

Utilities::Utilities(int indexInGame, int numOfPlayers)
{
    QVector<POSITIONS> directions;
    directions.push_back(POSITIONS::LEVO);
    directions.push_back(POSITIONS::GORE);
    directions.push_back(POSITIONS::DESNO);

    if(numOfPlayers == 2 && indexInGame == 0)
    {
        m_indexToPosition.insert(1,Utilities::POSITIONS::GORE);
    } else if (numOfPlayers == 2 && indexInGame == 1)
    {
        m_indexToPosition.insert(0, Utilities::POSITIONS::GORE);
    } else
    {
        int j = 0;
        for (int i = indexInGame +1; i < numOfPlayers; i++)
        {
            m_indexToPosition.insert(i, directions.at(j));
            j++;
        }
        for (int i = 0; i < indexInGame; i++)
        {
            m_indexToPosition.insert(i, directions.at(j));
            j++;
        }
    }
}

QPair<int, int> Utilities::expandCardIndex(int player, int cardIndex)
{
    int i, j;
    if(cardIndex == 4)
    {
        return qMakePair(0, 2);
    }
    POSITIONS position = m_indexToPosition[player];
    switch (position) {
    case POSITIONS::ME:
        if (cardIndex %  2 == 0)
        {
            j = 0;
            i = (cardIndex -2)/ -2;
        } else
        {
            j = 1;
            i = (cardIndex-3) / -2;
        }
        return qMakePair(i, j);
        break;
    case POSITIONS::LEVO:
        if (cardIndex %  2 == 0)
        {
            i = 0;
            j = cardIndex/2;
        } else
        {
            i = 1;
            j = (cardIndex-1) / 2;
        }
        return qMakePair(i, j);
    case POSITIONS::DESNO:
        if (cardIndex %  2 == 0)
        {
            i = 1;
            j = (cardIndex -2)/ -2;
        } else
        {
            i = 0;
            j = (cardIndex-3) / -2;
        }
        return qMakePair(i, j);
    case POSITIONS::GORE:
        if (cardIndex %  2 == 0)
        {
            j = 1;
            i = cardIndex/2;
        } else
        {
            j = 0;
            i = (cardIndex-1) / 2;
        }
        return qMakePair(i, j);
    default:
        throw "greška u expandCardIndex";
        break;
    }
}


int Utilities::flattenCardIndex(int player, int i, int j)
{
    if(i == 0 && j == 2)
    {
        return 4;
    }
    POSITIONS position = m_indexToPosition[player];
    switch (position) {
    case POSITIONS::ME:
        return 2-2*i+j;
        break;
    case POSITIONS::LEVO:
        return i + 2*j;
    case POSITIONS::DESNO:
        return 3-2*j-i;
    case POSITIONS::GORE:
        return 1-j+2*i;
    default:
        throw "greška u flattenCardIndex";
        break;
    }
}
