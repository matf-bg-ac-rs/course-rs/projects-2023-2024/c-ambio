#include "loginrequestpacket.h"

LoginRequestPacket::LoginRequestPacket(const QString &name, const int avatar)
    : Packet("login request")
    , m_name(name)
    , m_avatar(avatar)
{
}

QString LoginRequestPacket::name() const
{
    return m_name;
}

int LoginRequestPacket::avatar() const
{
    return m_avatar;
}
QByteArray LoginRequestPacket::toSend() const
{
    QJsonObject jsonObj;
    jsonObj.insert("type", "login request");
    jsonObj.insert("name", m_name);
    jsonObj.insert("avatar", m_avatar);

    QJsonDocument doc;
    doc.setObject(jsonObj);
    return doc.toJson();
}

