#ifndef FLYINTOREQUEST_H
#define FLYINTOREQUEST_H

#include "packet.h"

class FlyIntoRequest : public Packet
{
    Q_GADGET
public:
    explicit FlyIntoRequest(const int roundNum, const int flyIntoPlayer, const int flyIntoIndexOfCard);

    int roundNum() const;

    int flyIntoPlayer() const;

    int flyIntoIndexOfCard() const;

    QByteArray toSend() const override;

private:
    const int m_roundNum;
    const int m_flyIntoPlayer;
    const int m_flyIntoIndexOfCard;
};

#endif // FLYINTOREQUEST_H
