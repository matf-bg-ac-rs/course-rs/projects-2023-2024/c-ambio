#ifndef PACKET_H
#define PACKET_H

#include<QString>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

class Packet : public QObject
{
    Q_OBJECT
public:
    explicit Packet(const QString &packetType);

    virtual ~Packet() = default;
    virtual QByteArray toSend() const = 0;

    QString type() const;
private:
    QString m_type;

};

#endif // PACKET_H
