#include "readypacket.h"

ReadyPacket::ReadyPacket(const bool ready)
    : Packet("ready")
    , m_ready(ready)
{
}
bool ReadyPacket::ready() const
{
    return m_ready;
}
QByteArray ReadyPacket::toSend() const
{
    QJsonObject jsonObj;
    jsonObj.insert("type", "ready");
    jsonObj.insert("ready", m_ready);
    QJsonDocument doc;
    doc.setObject(jsonObj);
    return doc.toJson();
}
