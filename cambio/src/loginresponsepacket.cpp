#include "loginresponsepacket.h"

LoginResponsePacket::LoginResponsePacket(const int indexInGame, const bool isHost, const QVector<QPair<QString, int>> &connectedPlayers)
    : Packet("login response")
    , m_indexInGame(indexInGame)
    , m_isHost(isHost)
    , m_alreadyConnectedPlayers(connectedPlayers)
{}
bool LoginResponsePacket::isHost() const
{
    return m_isHost;
}
int LoginResponsePacket::indexInGame() const
{
    return m_indexInGame;
}
QByteArray LoginResponsePacket::toSend() const
{
    QJsonObject jsonObj;
    jsonObj.insert("type", "login response");
    jsonObj.insert("indexInGame", m_indexInGame);
    jsonObj.insert("isHost", m_isHost);

    QJsonArray jsonArray;
    for(const QPair<QString, int>& player : m_alreadyConnectedPlayers)
    {
        QJsonObject pairObject;
        pairObject.insert("playerName", player.first);
        pairObject.insert("playerAvatar", player.second);
        jsonArray.append(pairObject);
    }
    jsonObj.insert("players", jsonArray);

    QJsonDocument doc;
    doc.setObject(jsonObj);
    QByteArray bytes = doc.toJson();
    return bytes;
}

QVector<QPair<QString, int> > LoginResponsePacket::alreadyConnectedPlayers() const
{
    return m_alreadyConnectedPlayers;
}

