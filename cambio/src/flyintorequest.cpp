#include "flyintorequest.h"


FlyIntoRequest::FlyIntoRequest(const int roundNum, const int flyIntoPlayer, const int flyIntoIndexOfCard)
    :Packet("fly into")
    ,m_roundNum(roundNum)
    ,m_flyIntoPlayer(flyIntoPlayer)
    ,m_flyIntoIndexOfCard(flyIntoIndexOfCard)
{}

int FlyIntoRequest::roundNum() const
{
    return m_roundNum;
}

int FlyIntoRequest::flyIntoPlayer() const
{
    return m_flyIntoPlayer;
}

int FlyIntoRequest::flyIntoIndexOfCard() const
{
    return m_flyIntoIndexOfCard;
}

QByteArray FlyIntoRequest::toSend() const
{
    QJsonObject jsonObj;
    jsonObj.insert("type", "fly into");
    jsonObj.insert("roundNum", m_roundNum);
    jsonObj.insert("indexOfPlayer", m_flyIntoPlayer);
    jsonObj.insert("indexOfCard", m_flyIntoIndexOfCard);

    QJsonDocument doc;
    doc.setObject(jsonObj);
    return doc.toJson();
}
