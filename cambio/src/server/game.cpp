#include "game.h"

#include "../game/player.h"
#include "../game/deck.h"
#include "../game/pile.h"
#include "../game/card.h"

#include <QDebug>

Game* Game::instance = nullptr;

Game::Game(QObject *parent, const QVector<QString> &names)
    : QObject{parent}
    ,m_deck{nullptr}
    ,m_pile{nullptr}
    ,m_numOfPlayers{static_cast<int>(names.size())}
    ,m_indexOfPlayerWhoSaidCambio{-1}
{

    m_playersCards = QMap<int, QVector<QString> > ();
    m_scores.resize(names.size());
    for(int i = 0; i < names.size(); i++)
    {
        Player *player = new Player(names[i]);
        player->setIndexInGame(i);
        m_players.push_back(player);
    }
    initializationGame();
}

bool Game::isGameEnd() const
{
    return m_isGameEnd;
}

void Game::setIsGameEnd(bool newIsGameEnd)
{
    m_isGameEnd = newIsGameEnd;
}

Game* Game::getInstance(QObject *parent, const QVector<QString> &names)
{
    if (instance == nullptr)
    {
        instance = new Game(parent, names);
    }
    return instance;
}

Game::~Game()
{
    delete m_deck;
    delete m_pile;
    foreach(auto player, m_players)
        delete player;
}

void Game::resetInstance()
{
    delete instance;
    instance = nullptr;
}

void Game::initializationGame()
{
    if(m_deck!= nullptr && !(m_deck->isEmpty()))
        delete m_deck;
    m_deck = new Deck();
    m_deck->shuffle();

    if(m_pile!= nullptr && !(m_pile->isEmpty()))
        delete m_pile;
    m_pile = new Pile();
    m_pile->addCardToPile(m_deck->getFirstCard());

    m_indexOfPlayerWhoSaidCambio =-1;
    m_indexOfCurrentPlayer =0;
    m_roundNumber = 0;
    m_isFlyIntoInProgress = false;
    m_indexOfPlayerWhoseCardIsTaken = -1;
    m_indexOfTakenCard = -1;
    m_isPlayerHoldingDeckCard = false;
    m_isPlayerHoldingPileCard = false;
    m_indexOfPlayerWhoFlyIntoUsingOthersCard = -1;
    m_isGameEnd = false;
    dealHands();
}

void Game::dealHands()
{
    foreach(auto player, m_players)
    {
        player->setHand(m_deck->dealHand());
    }
}

QString Game::deckTop()
{
    try{
       auto tmp = m_deck->peekFirstCard()->toString();
        return tmp;
    }
    catch(std::exception e)
    {
       m_deck->refill(m_pile);
       auto tmp = m_deck->peekFirstCard()->toString();
       return tmp;
    }
}

QString Game::pileTop()
{
    return m_pile->peekFirstCard()->toString();
}

QString Game::throwCard()
{

    Card* newPile;
    try {
        newPile = m_deck->getFirstCard();

    }
    catch (std::exception e)
    {
        m_deck->refill(m_pile);
        newPile = m_deck->getFirstCard();
    }
    m_pile->addCardToPile(newPile);

    if(newPile->cardType().toLower() == "numeric")
        setIsPlayerHoldingDeckCard(false);
    return newPile->toString();
}

void Game::meSwap(const int indexOfPlayer, const int indexOfCardInHand)
{
    if(isPlayerHoldingDeckCard())
    {
        Player *me = m_players[indexOfPlayer];
        Card *newPile =  me->changeCardAtIndex(m_deck->getFirstCard(), indexOfCardInHand);
        m_pile->addCardToPile(newPile);
        //return newPile->toString();
    }else // if(isPlayerHoldingPileCard())
    {
        Player *me = m_players[indexOfPlayer];
        Card* newPile =  me->changeCardAtIndex(m_pile->getFirstCard(), indexOfCardInHand);
        m_pile->addCardToPile(newPile);
        //return newPile->toString();
    }
}

void Game::calculateScores()
{
    if(isGameOver())
    {
        int min = std::numeric_limits<int>::max();
        int minIndex = -1;
        int i = 0;
        for(auto &player : m_players)
        {
            QString name = player->name();
            player->calculateScore();
            m_scores[i++] = qMakePair(name, player->score());
            if(player->score() < min)
            {
                min = player->score();
                minIndex = player->indexInGame();
            }
        }

        if(m_indexOfPlayerWhoSaidCambio != -1 && minIndex != indexOfPlayerWhoSaidCambio())
        {
            for(auto &player : m_players)
            {
                QString name = player->name();
                for (QPair<QString, int> &score : m_scores)
                {
                    if (name == score.first)
                    {
                        if(player->indexInGame() == minIndex)
                        {
                            score.second = -5;
                        }else if(player->indexInGame() == indexOfPlayerWhoSaidCambio())
                        {
                            score.second = 10;
                        }else
                        {
                            score.second = 0;
                        }
                        break;
                    }
                }
            }

        }
        m_winnerIndex = minIndex;
    }
}



int Game::nextPlayer()
{
    m_indexOfCurrentPlayer += 1;
    m_indexOfCurrentPlayer %= m_numOfPlayers;
    return m_indexOfCurrentPlayer;
}

bool Game::isGameOver()
{
    return (m_indexOfPlayerWhoSaidCambio!=-1 && m_indexOfCurrentPlayer == m_indexOfPlayerWhoSaidCambio) || m_isGameEnd;
}
QString Game::eyeMove(const int indexOfPlayer, const int indexOfCardInHand) const
{
    Player *p = m_players[indexOfPlayer];
    Card *c = p->peekCardAtIndex(indexOfCardInHand);
    return c->toString();
}
QString Game::detectiveMove(const int indexOfPlayer, const int indexOfCardInHand) const
{
    Player *p = m_players[indexOfPlayer];
    Card *c = p->peekCardAtIndex(indexOfCardInHand);
    return c->toString();
}

void Game::swapMove(const int indexOfPlayer1, const int indexOfCardInHand1, const int indexOfPlayer2, const int indexOfCardInHand2)
{
    Player *p1 = m_players[indexOfPlayer1];
    Player *p2 = m_players[indexOfPlayer2];
    Card *c1 = p1->peekCardAtIndex(indexOfCardInHand1);
    Card *c2 = p2->changeCardAtIndex(c1, indexOfCardInHand2);
    p1->changeCardAtIndex(c2, indexOfCardInHand1);
}

void Game::holdCardMove(int indexOfPlayer)
{
    if(isPlayerHoldingDeckCard())
    {
        Player *me = m_players[indexOfPlayer];
        Card *deckCard;
        try {
            deckCard = m_deck->getFirstCard();

        }
        catch (std::exception e)
        {
            m_deck->refill(m_pile);
            deckCard = m_deck->getFirstCard();
        }
        me->addToHand(deckCard);
    }else // if(isPlayerHoldingPileCard())
    {
        Player *me = m_players[indexOfPlayer];
        me->addToHand(m_pile->getFirstCard());
    }
}

void Game::swapAndHold(const int indexOfPlayer1, const int indexOfCardInHand1, const int indexOfPlayer2)
{
    Player *p1 = m_players[indexOfPlayer1];
    Player *p2 = m_players[indexOfPlayer2];
    Card *c1 = p1->peekCardAtIndex(indexOfCardInHand1); //dohvatila sam ono sto je bilo na detective
    p2->addToHand(c1); //drugom igracu dodas ono sto je video
    Card *deckCard;
    try {
        deckCard = m_deck->getFirstCard();

    }
    catch (std::exception e)
    {
        m_deck->refill(m_pile);
        deckCard = m_deck->getFirstCard();
    }
    p1->changeCardAtIndex(deckCard, indexOfCardInHand1);//prvom igracu na mesto indexOfCardInHand1 dodaj kartu sa deck-a
}

bool Game::tryToFlyInto(const int indexOfPlayer, const int indexOfCardInHand)
{
    Player *p = m_players[indexOfPlayer];
    if(p->peekCardAtIndex(indexOfCardInHand)->cardNum() == m_pile->peekFirstCard()->cardNum())
        return true;
    else
        return false;
}

int Game::indexOfCurrentPlayer() const
{
    return m_indexOfCurrentPlayer;
}

int Game::indexOfPlayerWhoSaidCambio() const
{
    return m_indexOfPlayerWhoSaidCambio;
}

unsigned int Game::roundNumber() const
{
    return m_roundNumber;
}

int Game::incrementRoundNumber()
{
    m_roundNumber++;
    return m_roundNumber;
}

int Game::resetRoundNumber()
{
    if(isGameOver()){
        m_roundNumber = 0;
    }
    return m_roundNumber;
}

void Game::setIsPlayerHoldingDeckCard(bool newIsPlayerHoldingDeckCard)
{
    m_isPlayerHoldingDeckCard = newIsPlayerHoldingDeckCard;
}

void Game::setIsPlayerHoldingPileCard(bool newIsPlayerHoldingPileCard)
{
    m_isPlayerHoldingPileCard = newIsPlayerHoldingPileCard;
}

void Game::setIndexOfPlayerWhoSaidCambio(int newIndexOfPlayerWhoSaidCambio)
{
    m_indexOfPlayerWhoSaidCambio = newIndexOfPlayerWhoSaidCambio;
}

void Game::setIsGameOver(bool newIsGameOver)
{
    m_isGameOver = newIsGameOver;
}

Card* Game::playersCard(int indexOfPlayer, int indexOfCard) const
{
    Player *p = m_players[indexOfPlayer];
    return p->peekCardAtIndex(indexOfCard);
}

void Game::setPlayersCard(int indexOfPlayer, int indexOfCard, Card *newCard)
{
    Player *p = m_players[indexOfPlayer];
    p->changeCardAtIndex(newCard, indexOfCard);
}

int Game::givePlayerPenaltyCard(int indexOfPlayer)
{
    Player *p = m_players[indexOfPlayer];
    int index = 0;
    Card* card;

    for(int i = 0; i < p->numOfCards(); i++)
    {
        if(p->peekCardAtIndex(i) == nullptr)
        {
            index = i;
            try{
                card = m_deck->getFirstCard();
            }
            catch(std::exception e)
            {
                m_deck->refill(m_pile);
                card = m_deck->getFirstCard();
            }

            p->changeCardAtIndex(card, i);
            return index;
        }
    }

    index = p->numOfCards();
    try{
        card = m_deck->getFirstCard();
    }
    catch(std::exception e)
    {
        m_deck->refill(m_pile);
        card = m_deck->getFirstCard();
    }
    p->addToHand(card);
    return index;
}

bool Game::isFlyIntoInProgress() const
{
    return m_isFlyIntoInProgress;
}

void Game::setIsFlyIntoInProgress(bool newIsFlyIntoInProgress)
{
    m_isFlyIntoInProgress = newIsFlyIntoInProgress;
}

int Game::indexOfPlayerWhoseCardIsTaken() const
{
    return m_indexOfPlayerWhoseCardIsTaken;
}

void Game::setIndexOfPlayerWhoseCardIsTaken(int newIndexOfPlayerWhoseCardIsTaken)
{
    m_indexOfPlayerWhoseCardIsTaken = newIndexOfPlayerWhoseCardIsTaken;
}

int Game::indexOfTakenCard() const
{
    return m_indexOfTakenCard;
}

void Game::setIndexOfTakenCard(int newIndexOfTakenCard)
{
    m_indexOfTakenCard = newIndexOfTakenCard;
}

int Game::indexOfPlayerWhoFlyIntoUsingOthersCard() const
{
    return m_indexOfPlayerWhoFlyIntoUsingOthersCard;
}

void Game::setIndexOfPlayerWhoFlyIntoUsingOthersCard(int newIndexOfPlayerWhoFlyIntoUsingOthersCard)
{
    m_indexOfPlayerWhoFlyIntoUsingOthersCard = newIndexOfPlayerWhoFlyIntoUsingOthersCard;
}

QMap<int, QVector<QString> > Game::playersCards() const
{
    return m_playersCards;
}

int Game::winnerIndex() const
{
    return m_winnerIndex;
}

void Game::createMapOfPlayersCards()
{
    QMap<int, QVector<QString> > mapa;
    for (int i = 0; i < m_players.size(); i++)
    {
        Player* player = m_players[i];
        QVector<QString> cards;

        int playersHandSize = player->hand().size();
        for (int i = 0; i < playersHandSize; i++)
        {
            Card *card = player->peekCardAtIndex(i);
            if(card != nullptr)
            {
                cards.push_back(card->toString());
            }
        }
        mapa[player->indexInGame()]=cards;
    }
    m_playersCards = mapa;
}

QVector<QPair<QString, int> > Game::scores() const
{
    return m_scores;
}

bool Game::isPlayerHoldingDeckCard() const
{
    return m_isPlayerHoldingDeckCard;
}

bool Game::isPlayerHoldingPileCard() const
{
    return m_isPlayerHoldingPileCard;
}

Player *Game::playerAtIndex(const int i) const
{
    return m_players[i];
}

bool Game::isPlayerHoldingCard() const
{
    return m_isPlayerHoldingDeckCard || m_isPlayerHoldingPileCard;
}




