#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include<QTcpServer>
#include<QTcpSocket>
#include <QDir>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QString>


#include "game.h"

#include "../packet.h"
#include "../packetparser.h"
#include "../loginrequestpacket.h"
#include "../loginresponsepacket.h"
#include "../gamestatepacket.h"
#include "../gamestateresponse.h"
#include "../startgameresponse.h"
#include "../errorpacket.h"
#include "../readypacket.h"
#include "../movetype.h"


class Game;

class Server : public QObject
{
    Q_OBJECT

public:
    explicit Server(QObject *parent = nullptr);

        void sendToAll(const QByteArray &message);
        void sendToAllExcept(int index, const QByteArray &message);
        void sendMessage(QTcpSocket *socket, const QByteArray &msg);

        bool isStarted() const;
        int findSocketIndex(QTcpSocket *socket);

    signals:


    private slots:

        void client_connecting();
        void onClientReadReady();
        void onClientdisconnected();


        //    void onClientstateChanged();
        //    void onClienterrorOccurred();
        //    void onClientconnected();


    private:

        QTcpServer *m_server;
        QVector<QTcpSocket*> m_sockets;
        QVector<QString> m_names;
        QVector<int> m_avatars;
        QVector<bool> m_playersReady;
        bool m_isStarted; //server je pokrenut
        bool m_isGameStarted;
        bool m_readyIsSend;

        int m_maxPlayers; //choose one
        unsigned m_numOfMaxPlayers; //choose one

        int m_numOfConnectedPlayers; //choose one
        Game *m_game;

        QVector<QPair<QString, int>> m_connectedPlayers;

        //private metods
        bool isNameOk(const QString &name);
        void saveResults(const QVector<QPair<QString, int>> &roundScores);
        QString resultsPath();
        void endGame();
        void processTurn(int senderIndex, GameStatePacket *request);
        void processFlyInto(int senderIndex, FlyIntoRequest *request);
        void processLogin(int senderIndex,  QTcpSocket *socket, LoginRequestPacket *request);
        void processStartGame(int senderIndex, QTcpSocket *socket ,StartGameRequest *request);

};

#endif // SERVER_H
