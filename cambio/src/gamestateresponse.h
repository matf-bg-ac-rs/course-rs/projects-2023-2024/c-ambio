#ifndef GAMESTATERESPONSE_H
#define GAMESTATERESPONSE_H


#include "movetype.h"
#include "packet.h"

class GameStateResponse : public Packet
{
    Q_GADGET
public:
    GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer, const int playerCambio =-1);
    GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer, const QString &card, const int playerCambio =-1);
    GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer,
                      const QString &card, const int playerFrom, const int indexCardFromPlayer, const int playerCambio =-1);
    GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer,
                      const int playerFrom, const int indexCardFromPlayer, const int playerCambio =-1);
    GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer,
                      const int playerFrom, const int indexCardFromPlayer,
                      const int playerTo, const int indexCardToPlayer, const int playerCambio =-1);
    GameStateResponse(const MOVE_TYPE subtype, const int roundNum, const int indexOfCurrentPlayer,
                      const QString &card, const int playerFrom, const int indexCardFromPlayer,
                      const int playerTo, const int indexCardToPlayer, const int playerCambio =-1);


    ~GameStateResponse() = default;
    MOVE_TYPE subtype() const;

    int roundNum() const;

    int indexOfCurrentPlayer() const;

    int playerCambio() const;

    QString card() const;

    //QString newPile() const;

    int playerFrom() const;

    int indexCardFromPlayer() const;

    int playerTo() const;

    int indexCardToPlayer() const;

    QByteArray toSend() const override;


private:

    const MOVE_TYPE  m_subtype;
    const int m_roundNum;
    const int m_indexOfCurrentPlayer;
    const int m_playerCambio;
    const QString m_card;
    //const QString m_newPile;
    const int m_playerFrom;
    const int m_indexCardFromPlayer;
    const int m_playerTo;
    const int m_indexCardToPlayer;
};

#endif // GAMESTATERESPONSE_H
