#ifndef FLYINTORESPONSE_H
#define FLYINTORESPONSE_H

#include "packet.h"

class FlyIntoResponse : public Packet
{
    Q_OBJECT
public:
    explicit FlyIntoResponse(const int roundNum, const bool success,
                             const QString &card, const int cardOwner,
                             const int playerIndex, const int cardIndex,
                             const int indexOfPenaltyCard);

    //getters
    int roundNum() const;
    bool successFlyInto() const;
    QString card() const;
    QString playerName() const;
    int cardOwner() const;
    int playerIndex() const;
    int cardIndex() const;

    QByteArray toSend() const override;

    int indexOfPenaltyCard() const;

private:
    const int m_roundNum;
    const bool m_successFlyInto;
    const QString m_card;
    const int m_cardOwner;
    const int m_playerIndex;
    const int m_cardIndex;
    const int m_indexOfPenaltyCard;

};

#endif // FLYINTORESPONSE_H
