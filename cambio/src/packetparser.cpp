#include "packetparser.h"

#include <QMetaEnum>

Packet *PacketParser::parsePacket(const QByteArray& data)
{
    QJsonDocument jsonDoc = QJsonDocument::fromJson(data);
    QJsonObject jsonObj = jsonDoc.object();
    QString type = jsonObj["type"].toString();

    if (type == "login request")
    {
        QString name = jsonObj["name"].toString();
        int avatar = jsonObj["avatar"].toInt();
        return new LoginRequestPacket(name, avatar);
    }

    if (type == "login response")
    {
        int index = jsonObj["indexInGame"].toInt();
        bool isHost = jsonObj["isHost"].toBool();

        QJsonArray jsonArray = jsonObj["players"].toArray();
        QVector<QPair<QString, int>> players;
        for(const QJsonValue& value : jsonArray)
        {
            QJsonObject jsonObject = value.toObject();
            QString name = jsonObject["playerName"].toString();
            int avatar = jsonObject["playerAvatar"].toInt();
            players.append(qMakePair(name, avatar));
        }

        return new LoginResponsePacket(index, isHost, players);
    }

    if (type == "game state packet")
    {
        int subtypeInt = jsonObj["subtype"].toInt();
        MOVE_TYPE subtype = static_cast<MOVE_TYPE>(subtypeInt);

        return new GameStatePacket(subtype,
                                   jsonObj["roundNum"].toInt(),
                                   jsonObj["playerCambio"].toBool(),
                                   jsonObj["playerFrom"].toInt(),
                                   jsonObj["indexCardFromPlayer"].toInt(),
                                   jsonObj["playerTo"].toInt(),
                                   jsonObj["indexCardToPlayer"].toInt());
    }

    if(type == "game state response")
    {
        int subtypeInt = jsonObj["subtype"].toInt();
        MOVE_TYPE subtype = static_cast<MOVE_TYPE>(subtypeInt);
        QString card = (jsonObj.contains("newPile"))? jsonObj["newPile"].toString() : jsonObj["card"].toString();

        return new GameStateResponse(subtype,
                                    jsonObj["roundNum"].toInt(),
                                    jsonObj["indexOfCurrentPlayer"].toInt(),
                                    card,
                                    jsonObj["playerFrom"].toInt(),
                                    jsonObj["indexCardFromPlayer"].toInt(),
                                    jsonObj["playerTo"].toInt(),
                                    jsonObj["indexCardToPlayer"].toInt(),
                                    jsonObj["indexOfCambioPlayer"].toInt());
    }

    if(type == "fly into")
    {
        return new FlyIntoRequest(jsonObj["roundNum"].toInt(),
                                  jsonObj["indexOfPlayer"].toInt(),
                                  jsonObj["indexOfCard"].toInt());
    }
    if (type == "start game request")
    {
        QString name = jsonObj["name"].toString();
        bool isHost = jsonObj["isHost"].toBool();
        int indexInGame = jsonObj["indexInGame"].toInt();
        return new StartGameRequest(isHost, name, indexInGame);
    }
    if (type == "start game response")
    {
        QJsonArray jsonNameArray = jsonObj["namesOfPlayers"].toArray();
        QJsonArray jsonAvatarArray = jsonObj["avatarsOfPlayers"].toArray();
        QJsonArray jsonFirstTwoCards = jsonObj["firstTwoCards"].toArray();
        QVector<QString> names;
        QVector<int> avatars;
        QVector<QString> cards;
        for (const QJsonValue &name : jsonNameArray)
        {
            names.append(name.toString());
        }
        for (const QJsonValue &avatar: jsonAvatarArray)
        {
            avatars.append(avatar.toInt());
        }
        for (const QJsonValue &card: jsonFirstTwoCards)
        {
            cards.append(card.toString());
        }

        QString pileTop = jsonObj["pileTop"].toString();
        return new StartGameResponse(names, avatars, cards[0], cards[1], pileTop);
    }
    if (type == "error")
    {
        QString message = jsonObj["message"].toString();
        return new ErrorPacket(message);
    }

    if (type == "flyinto response")
    {
        return new FlyIntoResponse(jsonObj["roundNum"].toInt(),
                                   jsonObj["successFlyInto"].toBool(),
                                   jsonObj["card"].toString(),
                                   jsonObj["cardOwner"].toInt(),
                                   jsonObj["playerIndex"].toInt(),
                                   jsonObj["cardIndex"].toInt(),
                                   jsonObj["indexOfPenaltyCard"].toInt()
                                   );
    }
    if (type == "ready")
    {
        return new ReadyPacket(jsonObj["ready"].toBool());
    }
    if (type == "end game")
    {
        QVector<QPair<QString, int>> result;
        QJsonArray jsonArray = jsonObj["results"].toArray();
        for (const QJsonValue &value : jsonArray)
        {
            QJsonObject jsonObject = value.toObject();
            QString name = jsonObject["playerName"].toString();
            int score = jsonObject["playerScore"].toInt();
            result.push_back(qMakePair(name, score));
        }

        int winnerIndex = jsonObj["winnerIndex"].toInt();

        QJsonObject jsonMap = jsonObj["playersCards"].toObject();
        QMap<int, QVector<QString>> playersCards;

        QJsonObject::const_iterator it = jsonMap.constBegin();
        while(it != jsonMap.constEnd())
        {
            int key = it.key().toInt();
            QJsonArray cardsArray = it.value().toArray();
            QVector<QString> cards;
            for(int i = 0; i < cardsArray.size(); i++)
                cards.push_back(cardsArray[i].toString());

            playersCards.insert(key, cards);
            it++;
        }
        return new EndGame(result, playersCards, winnerIndex);
    }
    return nullptr;
}
