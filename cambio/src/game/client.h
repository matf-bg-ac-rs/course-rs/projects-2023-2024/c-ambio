#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QString>
#include <QTcpSocket>
#include <QApplication>
#include <QHostAddress>

#include "../packet.h"
#include "../packetparser.h"
#include "../loginrequestpacket.h"
#include "../loginresponsepacket.h"
#include "../startgamerequest.h"
#include "../startgameresponse.h"
#include "../errorpacket.h"
#include "../readypacket.h"
#include "../movetype.h"

#include "clientgamestate.h"

class Client : public QObject
{
    Q_OBJECT
public:
    explicit Client (QObject *parent, const QString &name, int avatar);

    Client(QObject *parent);

    //getters
    QString name() const;
    int indexInGame() const;
    QTcpSocket *socketClient() const;
    bool isHost() const;

    //setters
    void setIsHost(bool newIsHost);
    void setIndexInGame(int newIndexInGame);
    void setName(const QString &newName);
    void sendMessage(QByteArray msg);

    int avatar() const;

    QVector<QPair<QString, int> > alreadyConnectedPlayers() const;

signals:
    void connected();
    void disconnected();
    void readyRead();
    void newPlayerConnected(const QString& name, const int avatar);
    void connectedPlayers(const QVector<QPair<QString, int>>& players);
    void playerChoseDeck(const QString& name, const QString& card);
    void playerChosePile(const QString& name, const QString& card);
    void startGameGui(ClientGameState *myGameState);
    void errorMessage(const QString& message);
    void signalEndGame(const QVector<QPair<QString, int>> result, const QMap<int, QVector<QString>> playersCards, const int winnersIndex);

public slots:
    void onConnected();
    void onDisconnected();
    void onReadyToRead();
    void onDataReady(const QString& data);

    void onStartGameRequest();
    void onPlayerChoseDeck();
    void onPlayerChosePile();

    void onStateHasChanged(MOVE_TYPE);
    void onReady();

    void onReturnToMainMenu();

private:
    QTcpSocket *m_socketClient;
    QString m_name;
    int m_avatar;
    bool m_isHost;
    int m_indexInGame;
    QVector<QPair<QString, int>> m_alreadyConnectedPlayers;
    const QHostAddress m_hostAddr = QHostAddress::LocalHost;
    int m_port = 62133;
    bool m_gameStarted = false;

    ClientGameState *m_myGameState;

    void processGameStateResponse(GameStateResponse* response);
    void processFlyIntoResponse(FlyIntoResponse* response);
};

#endif // CLIENT_H

