#include "lobby.h"
#include "ui_lobby.h"

Lobby::Lobby(Client *client, QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Lobby)
    , m_client(client)
{
    m_client = client;
    ui->setupUi(this);
    this->setWindowTitle("Lobi");

    m_clientConnected.resize(4, false);
    m_frames.resize(4);
    m_avatars.resize(4);
    m_names.resize(4);

    renderLobby();

    m_mediaPlayer = new QMediaPlayer();
    m_audioOutput = new QAudioOutput();
    m_mediaPlayer->setAudioOutput(m_audioOutput);
    m_mediaPlayer->setSource(QUrl::fromUserInput("qrc:/Sounds/lobbySound.mp3"));
    m_mediaPlayer->play();

    QObject::connect(ui->btnStartTheGame, &QPushButton::clicked, m_client,
                     &Client::onStartGameRequest);

    QObject::connect(ui->tbtnLobbySound, &QToolButton::clicked, this,
                     &Lobby::onTbtnLobbySoundClicked);

    QObject::connect(m_client, &Client::newPlayerConnected, this,
                     &Lobby::onNewPlayerConnected);

    QObject::connect(m_client, &Client::connectedPlayers, this,
                     &Lobby::onConnectedPlayers);

    QObject::connect(m_client, &Client::startGameGui, this,
                     &Lobby::onBtnStartGameClicked);

    QObject::connect(m_client, &Client::errorMessage, this,
                     &Lobby::onErrorMessage);

    QObject::connect(m_mediaPlayer, &QMediaPlayer::mediaStatusChanged, m_mediaPlayer,
                     &QMediaPlayer::play);

}

Lobby::~Lobby()
{
    delete ui;
}

void Lobby::onTbtnLobbySoundClicked()
{
    if (m_volume)
    {
        m_audioOutput->setMuted(true);
        QPixmap pixmap(":/Icons/noSound.png");
        QIcon buttonIcon(pixmap);
        ui->tbtnLobbySound->setIcon(buttonIcon);
        ui->tbtnLobbySound->setIconSize(QSize(50, 50));

    }
    else
    {
        m_audioOutput->setMuted(false);
        QPixmap pixmap(":/Icons/sound.png");
        QIcon buttonIcon(pixmap);
        ui->tbtnLobbySound->setIcon(buttonIcon);
        ui->tbtnLobbySound->setIconSize(QSize(50, 50));
    }
    m_volume = !m_volume;
}

void Lobby::onBtnStartGameClicked(ClientGameState *myGameState)
{
    GameGui *gameGui = new GameGui(myGameState);
    gameGui->show();

    delete m_audioOutput;
    delete m_mediaPlayer;
    this->close();
}

void Lobby::onErrorMessage(const QString message)
{
    auto *lblMessage = new QLabel(this);
    lblMessage->setText(message);
    QFont font("Ubuntu Condensed", 15, QFont::Bold);
    lblMessage->setFont(font);
    QPalette palette = lblMessage->palette();
    palette.setColor(QPalette::WindowText, Qt::red);
    lblMessage->setPalette(palette);
    lblMessage->setAlignment(Qt::AlignCenter);
    lblMessage->setDisabled(true);
    int lblHeight = lblMessage->height();
    int lblWidth = 300;
    lblMessage->setGeometry(m_screenWidth / 2 - lblWidth / 2, m_screenHeight - 80 - lblHeight, lblWidth, lblHeight);
    lblMessage->show();

}

void Lobby::initMainPlayer()
{
    int frameWidth = 180;
    int frameHeight = 300;
    auto *frame = new QFrame(this);
    frame->setGeometry(m_screenWidth / 2 - frameWidth , m_screenHeight / 2 - frameHeight / 2,
                       frameWidth, frameHeight);
    frame->setStyleSheet("QFrame {background-color: transparent}"
                         "{background: none}"
                         "{border: none}"
                         "{background-repeat: none}");
    frame -> setLayout(new QVBoxLayout);

    auto *btnAvatar = new QPushButton(this);
    int avatarSize = 150;
    btnAvatar->setFixedSize(avatarSize, avatarSize);
    int avatar = m_client->avatar();
    QString ss = QStringLiteral("QPushButton {border-image: url(:/Icons/avatar%1.png); background-color: transparent}").arg(avatar);
    btnAvatar->setStyleSheet(ss);
    btnAvatar->setDisabled(true);
    m_avatars[0] = btnAvatar;

    auto *lblName = new QLabel(this);
    int nameSize = 150;
    lblName->setFixedSize(nameSize, 50);
    QString name = m_client->name();
    lblName->setText(name);
    QFont font("Ubuntu Condensed", 15, QFont::Bold);
    lblName->setFont(font);
    QPalette palette = lblName->palette();
    palette.setColor(QPalette::WindowText, Qt::white);
    lblName->setPalette(palette);
    lblName->setAlignment(Qt::AlignCenter);
    lblName->setDisabled(true);
    m_names[0] = lblName;


    auto *vBoxLayout = qobject_cast<QVBoxLayout *>(frame->layout());
    vBoxLayout->insertWidget(0, lblName);
    vBoxLayout->insertWidget(0, btnAvatar);

    m_clientConnected[0] = true;
    m_frames[0] =frame;
}

void Lobby::initPlayer(int i, const QString &name, int avatar)
{
    int frameWidth = 180;
    int frameHeight = 300;
    auto *frame = new QFrame(this);
    frame->setStyleSheet("QFrame {background-color: transparent}"
                         "{background: none}"
                         "{border: none}"
                         "{background-repeat: none}");
    frame -> setLayout(new QVBoxLayout);

    auto *btnAvatar = new QPushButton(this);
    int avatarSize = 150;
    btnAvatar->setFixedSize(avatarSize, avatarSize);
    QString ss = QStringLiteral("QPushButton {border-image: url(:/Icons/avatar%1.png); background-color: transparent}").arg(avatar);
    btnAvatar->setStyleSheet(ss);
    btnAvatar->setDisabled(true);
    m_avatars[i] = btnAvatar;

    auto *lblName = new QLabel(this);
    int nameSize = 150;
    lblName->setFixedSize(nameSize, 50);
    lblName->setText(name);
    QFont font("Ubuntu Condensed", 15, QFont::Bold);
    lblName->setFont(font);
    QPalette palette = lblName->palette();
    palette.setColor(QPalette::WindowText, Qt::white);
    lblName->setPalette(palette);
    lblName->setAlignment(Qt::AlignCenter);
    lblName->setDisabled(true);
    m_names[i] = lblName;

    auto *vBoxLayout = qobject_cast<QVBoxLayout *>(frame->layout());
    vBoxLayout->insertWidget(0, lblName);
    vBoxLayout->insertWidget(0, btnAvatar);

    m_frames[i] = frame;
}

void Lobby::renderLobby()
{
    int btnWidth = ui->btnStartTheGame->width();
    int btnHeight = ui->btnStartTheGame->height();
    ui->btnStartTheGame->setGeometry(m_screenWidth / 2 - btnWidth / 2, m_screenHeight - btnHeight - 30, btnWidth, btnHeight);


    initLobby();

}

void Lobby::initLobby()
{
    int frameWidth = m_screenWidth;
    int frameHeight = 300;
    auto *frame = new QFrame(this);
    frame->setGeometry(0 , m_screenHeight / 2 - frameHeight / 2,
                       frameWidth, frameHeight);
    frame->setStyleSheet("QFrame {background-color: transparent}"
                         "{background: none}"
                         "{border: none}"
                         "{background-repeat: none}");
    m_layout = new QHBoxLayout();
    frame->setLayout(m_layout);

    initMainPlayer();
    m_layout->insertWidget(0, m_frames[0]);

    for(int i = 1; i < 4; i++){
        initPlayer(i);
        m_layout->insertWidget(i, m_frames[i]); //MEM CHECK: OVDE PRIJAVLJUJE CURENJE MEMORIJE!
    }

    frame->show();

}

void Lobby::showStartGameButton()
{
    if(m_client->isHost())
    {
        ui->btnStartTheGame->setDisabled(false);
    }else
    {
        ui->btnStartTheGame->hide();
    }
}

void Lobby::onNewPlayerConnected(const QString name, const int avatar)
{
    for (int i = 0; i < 4; i++)
    {
        if (!m_clientConnected[i])
        {
            m_clientConnected[i] = true;
            initPlayer(i, name, avatar);
            auto oldWidget = m_layout->itemAt(i)->widget();
            m_layout->replaceWidget(oldWidget, m_frames[i]);
            oldWidget->deleteLater();
            break;
        }
    }

}

void Lobby::onConnectedPlayers(const QVector<QPair<QString, int> > players)
{
    showStartGameButton();

    int j = 0;
    for (int i = 0; i < 4; i++)
    {
        if (!m_clientConnected[i])
        {
            if (j >= players.size())
                break;
            m_clientConnected[i] = true;
            initPlayer(i, players[j].first, players[j].second);
            j++;
            auto oldWidget = m_layout->itemAt(i)->widget();
            m_layout->replaceWidget(oldWidget, m_frames[i]);
            oldWidget->deleteLater();
        }
    }
}

