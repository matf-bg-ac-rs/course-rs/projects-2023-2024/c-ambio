#include "magiccard.h"
#include "card.h"

MagicCard::MagicCard() = default;

MagicCard::MagicCard(int cardNum)
    : Card(cardNum),
    m_magicType(magicTypeFromNum(cardNum))
{
}

MagicCard::~MagicCard() = default;

auto MagicCard::magicType() const -> MagicCard::MAGIC_TYPE { return m_magicType; }
//void MagicCard::setCardMagicType(MagicCard::MAGIC_TYPE mt) { m_magicType = mt; }
auto MagicCard::magicTypeFromNum(int cardNum) const -> MagicCard::MAGIC_TYPE
{
    switch (cardNum)
    {
    case 7:
        return MAGIC_TYPE::EYE;
        break;
    case 8:
        return MAGIC_TYPE::DETECTIVE;
        break;
    case 9:
        return MAGIC_TYPE::SWAP;
        break;
    case 10:
        return MAGIC_TYPE::LOOK_AND_SWAP;
        break;
    default:
        throw "Given number cannot be applied to magic card!";
        break;
    }
}

auto MagicCard::toString() const -> QString
{
    //magic:Detective:8
    QString magic;
    int num = this->cardNum();
    switch (m_magicType)
    {
    case MAGIC_TYPE::EYE:
        magic = "Look";
        break;
    case MAGIC_TYPE::DETECTIVE:
        magic = "Detective";
        break;
    case MAGIC_TYPE::SWAP:
        magic = "Swap";
        break;
    case MAGIC_TYPE::LOOK_AND_SWAP:
        magic = "LookAndSwap";
        break;
    default:
        throw "Unknown";
    }
    return QStringLiteral("magic:%1:%2").arg(magic).arg(num);
}

QString MagicCard::cardType() const
{   QString magic;
    switch (m_magicType)
    {
    case MAGIC_TYPE::EYE:
        magic = "Look";
        break;
    case MAGIC_TYPE::DETECTIVE:
        magic = "Detective";
        break;
    case MAGIC_TYPE::SWAP:
        magic = "Swap";
        break;
    case MAGIC_TYPE::LOOK_AND_SWAP:
        magic = "LookAndSwap";
        break;
    default:
        throw "Unknown";
    }
    return magic;
}
