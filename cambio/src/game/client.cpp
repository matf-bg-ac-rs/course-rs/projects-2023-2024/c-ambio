#include "client.h"

Client::Client(QObject *parent)
    : QObject{parent}
{
    m_isHost = false;
    m_socketClient = new QTcpSocket();
    connect(m_socketClient, &QTcpSocket::connected, this, &Client::onConnected);
    connect(m_socketClient, &QTcpSocket::disconnected, this, &Client::onDisconnected);
    connect(m_socketClient, &QTcpSocket::readyRead, this, &Client::onReadyToRead);

    connect(m_socketClient, &QTcpSocket::connected, this, &Client::connected);
    connect(m_socketClient, &QTcpSocket::disconnected, this, &Client::disconnected);
    connect(m_socketClient, &QTcpSocket::readyRead, this, &Client::readyRead);
}


Client::Client(QObject *parent, const QString &name, int avatar)
    : QObject{parent}
{
    m_name = name;
    m_avatar = avatar;
    m_isHost = false;
    m_socketClient = new QTcpSocket();

    connect(m_socketClient, &QTcpSocket::connected, this, &Client::onConnected);
    connect(m_socketClient, &QTcpSocket::disconnected, this, &Client::onDisconnected);
    connect(m_socketClient, &QTcpSocket::readyRead, this, &Client::onReadyToRead);

    connect(m_socketClient, &QTcpSocket::connected, this, &Client::connected);
    connect(m_socketClient, &QTcpSocket::disconnected, this, &Client::disconnected);
    connect(m_socketClient, &QTcpSocket::readyRead, this, &Client::readyRead);

    m_socketClient->connectToHost(m_hostAddr, m_port);

}

void Client::sendMessage(QByteArray msg)
{
    m_socketClient->write(msg);
    m_socketClient->flush();
}

void Client::onReadyToRead()
{

    QByteArray data = m_socketClient->readAll();

    Packet *p = PacketParser::parsePacket(data);

    if (LoginResponsePacket *response = dynamic_cast<LoginResponsePacket*>(p))
    {
        m_isHost = response->isHost();
        m_indexInGame = response->indexInGame();
        m_alreadyConnectedPlayers = response->alreadyConnectedPlayers();
        emit connectedPlayers(m_alreadyConnectedPlayers);
        response->deleteLater();
    }
    if (LoginRequestPacket *newPlayer = dynamic_cast<LoginRequestPacket*>(p))
    {
        emit newPlayerConnected(newPlayer->name(), newPlayer->avatar());
        newPlayer->deleteLater();
    }
    if (StartGameResponse *startResponse = dynamic_cast<StartGameResponse*>(p))
    {
        QVector<QString> playersNames = startResponse->namesOfPlayers();
        QVector<int> playersAvatars = startResponse->avatarsOfPlayers();
        QVector<QString> myTwoCards = startResponse->firstTwoCards();
        QString pile = startResponse->pileTop();


        m_myGameState = new ClientGameState(playersNames, playersAvatars, m_indexInGame, m_isHost, pile);

        QObject::connect(m_myGameState, &ClientGameState::signalMoveType, this,
                         &Client::onStateHasChanged);
        QObject::connect(m_myGameState, &ClientGameState::signalReady,this,
                         &Client::onReady);
        QObject::connect(this, &Client::signalEndGame,m_myGameState,
                         &ClientGameState::signalEndGame);

        QObject::connect(m_myGameState, &ClientGameState::signalReturnToMainMenu, this,
                         &Client::onReturnToMainMenu);



        m_myGameState->setMyTwoCards(myTwoCards);


        emit startGameGui(m_myGameState);

        startResponse->deleteLater();
    }
    if (ErrorPacket *errorResponse = dynamic_cast<ErrorPacket*>(p))
    {
        emit errorMessage(errorResponse->msg());
        errorResponse->deleteLater();
    }
    if (GameStateResponse *response = dynamic_cast<GameStateResponse*>(p))
    {
        processGameStateResponse(response);
    }

    if(FlyIntoResponse *response = dynamic_cast<FlyIntoResponse*>(p))
    {
        processFlyIntoResponse(response);
    }
    if (ReadyPacket *ready = dynamic_cast<ReadyPacket*>(p))
    {

        m_myGameState->setReadyToStart(true);
        m_myGameState->stateHasChanged(MOVE_TYPE::READY);
        ready->deleteLater();
    }
    if (EndGame *response = dynamic_cast<EndGame*>(p))
    {
        emit signalEndGame(response->result(),response->playersCards(),response->winnersIndex());
        response->deleteLater();
    }
    if (p != nullptr)
        p->deleteLater();
}
void Client::onStartGameRequest()
{
    StartGameRequest *start = new StartGameRequest(m_isHost, m_name, m_indexInGame);
    sendMessage(start->toSend());
    start->deleteLater();
}

void Client::onPlayerChoseDeck()
{
    sendMessage("pcd:");
}
void Client::onPlayerChosePile()
{
    sendMessage("pcp:");
}

void Client::onStateHasChanged(MOVE_TYPE move)
{
    if(move == MOVE_TYPE::END_GAME)
    {
        EndGame message = EndGame();
        sendMessage(message.toSend());
    }else if(move == MOVE_TYPE::FLY_INTO)
    {
        FlyIntoRequest message = FlyIntoRequest(m_myGameState->turnNumber(), m_myGameState->choosenPlayerIndex1(), m_myGameState->choosenCardIndex1());

        sendMessage(message.toSend());
    }else if( move == MOVE_TYPE::HAND_OVER)
    {
        FlyIntoRequest message = FlyIntoRequest(m_myGameState->turnNumber(), m_myGameState->choosenPlayerIndex2(), m_myGameState->choosenCardIndex2());
        sendMessage(message.toSend());
    }else
    {
        GameStatePacket message = GameStatePacket(move, m_myGameState->turnNumber(), m_myGameState->sayCambio(), m_myGameState->choosenPlayerIndex1(), m_myGameState->choosenCardIndex1(), m_myGameState->choosenPlayerIndex2(), m_myGameState->choosenCardIndex2());
        sendMessage(message.toSend());
    }
}
void Client::onReady()
{
    m_myGameState->setReadyToStart(true);
    ReadyPacket message = ReadyPacket(m_myGameState->readyToStart());
    sendMessage(message.toSend());
}

void Client::onReturnToMainMenu()
{
//    m_myGameState->restartMyState();
//    m_myGameState->restartFlyInto();
//    m_myGameState->setReadyToStart(false);
    m_socketClient->close();
}

void Client::processFlyIntoResponse(FlyIntoResponse *response)
{
    bool success = response->successFlyInto();
    int cardOwner = response->cardOwner();
    int playerIndex = response->playerIndex();


    if(success)//uspesno uletanje nebitno mojom ili tudjom iste se stvari postavljaju
    {
        if(cardOwner != playerIndex && response->card() == "") //znaci da hoces da predas kartu
        {
            //ovde ne dodajem u vektor uletanja jer je uletanje vec bilo
            m_myGameState->setHandOverCardIndex(response->cardIndex());
            m_myGameState->setFlyIntoIndexOfTakenCard(response->indexOfPenaltyCard());

            m_myGameState->stateFlyInto(success, MOVE_TYPE::HAND_OVER);
            //podesavanje broja karata svakog igraca
        }else{


            m_myGameState->setFlyInto(true);
            m_myGameState->incrementNumOfFlyInto();
            m_myGameState->setIndexOfPlayerWhoFlyInto(playerIndex);
            m_myGameState->setFlyIntoSuccessCard(response->card());
            m_myGameState->setFlyIntoIndexOfPlayerWhoseCardIsTaken(cardOwner);
            m_myGameState->setFlyIntoIndexOfTakenCard(response->cardIndex());
            m_myGameState->setPileCard(response->card());

            m_myGameState->decrementPlayerNumOfCards(playerIndex);
            m_myGameState->stateFlyInto(success);
        }

    }else if(!success) //neuspesno uletanje mojom kartom
    {
        m_myGameState->incrementNumOfFlyInto();
        m_myGameState->setFlyIntoFailurePlayer(playerIndex);
        m_myGameState->setFlyIntoFailureOwner(cardOwner);
        m_myGameState->setFlyIntoFailureCardIndex(response->cardIndex());
        m_myGameState->setFlyIntoFailureCard(response->card());
        m_myGameState->setIndexOfPenaltyCard(response->indexOfPenaltyCard());

        m_myGameState->incrementPlayerNumOfCards(playerIndex);
        m_myGameState->stateFlyInto(success);
    }


    if(response!=nullptr)
        response->deleteLater();
}

void Client::processGameStateResponse(GameStateResponse *response)
{
    MOVE_TYPE move = response->subtype();
    //uvek se salje roundNum i indexOfCurrentPlayer
    m_myGameState->setTurnNumber(response->roundNum());
    int indexOfCurrentPlayer = response->indexOfCurrentPlayer();
    if(m_myGameState->indexOfCurrentPlayer() != indexOfCurrentPlayer)
        m_myGameState->setIsIndexOfCurrentPlayerHasChanged(true);
    m_myGameState->setIndexOfCurrentPlayer(indexOfCurrentPlayer);
    switch(move){
    case MOVE_TYPE::DECK:
    {

        if(m_myGameState->isMyTurn() == true)
        {
            //setovala sam koju kartu drzim u ruci
            Card* cardInHand = Card::createCard(response->card());
            if(cardInHand->cardType().toLower() != "numeric")
                m_myGameState->setIsMagic(true);
            delete cardInHand;
            m_myGameState->setCardInMyHand(response->card());

        }
        m_myGameState->restartFlyInto();
        m_myGameState->setIsHoldingCardInHand(true);
        m_myGameState->stateHasChanged(move);
        break;
    }
    case MOVE_TYPE::PILE:
    {
        if(m_myGameState->isMyTurn() == true)
        {
            //setovala sam koju kartu drzim u ruci - to je ista karta koja je na pile-u
            m_myGameState->setCardInMyHand(QString(m_myGameState->pileCard()));
        }
        m_myGameState->setIsHoldingCardInHand(true);
        m_myGameState->stateHasChanged(move);
        break;
    }
    case MOVE_TYPE::THROW_CARD:
    {
        m_myGameState->setPileCard(response->card());
        if(m_myGameState->isMyTurn() == true)
            m_myGameState->setCardInMyHand("");
        m_myGameState->setIndexOfPlayerWhoSaidCambio(response->playerCambio());
        Card *card = Card::createCard(response->card());
        if(card->cardType().toLower() == "numeric")
        {
            m_myGameState->setIsHoldingCardInHand(false);
        }
        delete card;

        m_myGameState->stateHasChanged(move);
        break;
    }
    case MOVE_TYPE::ME_SWAP: //napomena: me_swap vise nije kraj poteza vec end_watch
    {
        if(m_myGameState->isMyTurn() && m_myGameState->isMagic()){
            m_myGameState->setIsMagic(false);
        }

        m_myGameState->setChoosenPlayerIndex1(response->playerFrom());
        m_myGameState->setChoosenCardIndex1(response->indexCardFromPlayer());

        m_myGameState->stateHasChanged(move);
        break;
    }
    case MOVE_TYPE::EYE_MOVE:
    {
        if(m_myGameState->isMyTurn())
        {
            m_myGameState->setCardInMyHand(response->card());
        }
        m_myGameState->setChoosenPlayerIndex1(response->playerFrom());
        m_myGameState->setChoosenCardIndex1(response->indexCardFromPlayer());

        m_myGameState->stateHasChanged(move);
        break;
    }
    case MOVE_TYPE::DETECTIVE_MOVE:
    {
        if(m_myGameState->isMyTurn())
        {
            m_myGameState->setCardInMyHand(response->card());
        }
        m_myGameState->setChoosenPlayerIndex1(response->playerFrom());
        m_myGameState->setChoosenCardIndex1(response->indexCardFromPlayer());

        m_myGameState->stateHasChanged(move);
        break;
    }
    case MOVE_TYPE::SWAP_MOVE:
    {

        m_myGameState->setChoosenPlayerIndex1(response->playerFrom());
        m_myGameState->setChoosenCardIndex1(response->indexCardFromPlayer());
        m_myGameState->setChoosenPlayerIndex2(response->playerTo());
        m_myGameState->setChoosenCardIndex2(response->indexCardToPlayer());


        m_myGameState->stateHasChanged(move);
        break;
    }
    case MOVE_TYPE::END_WATCH:
    {
        m_myGameState->setPileCard(response->card());
        if(m_myGameState->isMyTurn() == true)
            m_myGameState->setCardInMyHand("");
        m_myGameState->setIndexOfPlayerWhoSaidCambio(response->playerCambio());
        m_myGameState->setIsHoldingCardInHand(false);

        m_myGameState->stateHasChanged(move);
        break;
    }
    case MOVE_TYPE::READY:
    {
        break;
    }
    case MOVE_TYPE::HOLD_CARD:
    {
        m_myGameState->setPileCard(response->card());
        if(m_myGameState->isMyTurn() == true)
            m_myGameState->setCardInMyHand("");
        m_myGameState->setIndexOfPlayerWhoSaidCambio(response->playerCambio());
        m_myGameState->setIsHoldingCardInHand(false);

        m_myGameState->setChoosenPlayerIndex1(response->playerFrom());
        m_myGameState->setChoosenCardIndex1(response->indexCardFromPlayer());
        m_myGameState->setChoosenPlayerIndex2(response->playerTo());
        m_myGameState->setChoosenCardIndex2(response->indexCardToPlayer());

        m_myGameState->stateHasChanged(move);
        break;
    }
    default:
    {
        throw "Invalid move type!";
    }
    }
    if(response!=nullptr)
        response->deleteLater();
}

QVector<QPair<QString, int> > Client::alreadyConnectedPlayers() const
{
    return m_alreadyConnectedPlayers;
}

int Client::avatar() const
{
    return m_avatar;
}

void Client::onDataReady(const QString& data)
{
    m_socketClient->write(data.toUtf8());
}

void Client::onDisconnected()
{
//    QApplication::quit();
}

void Client::onConnected()
{
    LoginRequestPacket *p = new LoginRequestPacket(m_name, m_avatar);
    sendMessage(p->toSend());
    p->deleteLater();
}

int Client::indexInGame() const
{
    return m_indexInGame;
}

QTcpSocket *Client::socketClient() const
{
    return m_socketClient;
}

bool Client::isHost() const
{
    return m_isHost;
}

QString Client::name() const
{
    return m_name;
}

void Client::setIndexInGame(int newIndexInGame)
{
    m_indexInGame = newIndexInGame;
}
void Client::setIsHost(bool newIsHost)
{
    m_isHost= newIsHost;
}
void Client::setName(const QString &newName)
{
    m_name = newName;
}
