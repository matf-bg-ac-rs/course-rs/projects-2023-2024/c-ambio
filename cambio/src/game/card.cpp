#include "card.h"
#include "regularcard.h"
#include "magiccard.h"

Card::Card() = default;

Card::Card(int cardNum)
    : m_cardNum(cardNum)
{}

void Card::setCardNum(int newCardNum)
{
    m_cardNum = newCardNum;
}

Card::~Card() = default;

const QSet<int> Card::validRegularNumbers = {-2,0,1,2,3,4,5,6, 15};
const QSet<int> Card::validMagicNumbers = {7,8,9,10};
QString Card::m_color = "pink";

bool Card::isValid(int num, const QSet<int> &validNumbers)
{
    return validNumbers.find(num) != validNumbers.end();
}

QString Card::color()
{
    return m_color;
}

auto Card::cardNum() const -> int { return m_cardNum; }

bool Card::operator==(const Card &card) const
{
    return m_cardNum == card.cardNum();
}

bool Card::operator!=(const Card &card) const
{
    return m_cardNum != card.cardNum();
}

Card *Card::createCard(int cardNum)
{
    if(Card::isValid(cardNum, Card::validRegularNumbers))
    {
        RegularCard::CARD_COLOR validColor;
        if(Card::m_color == "pink")
            validColor = RegularCard::CARD_COLOR::PINK;
        else if(Card::m_color == "blue")
            validColor = RegularCard::CARD_COLOR::BLUE;
        else
            throw "Invalid color";

        return new RegularCard(cardNum, validColor);
    }else if(Card::isValid(cardNum, Card::validMagicNumbers))
    {
        return new MagicCard(cardNum);
    }else{
        throw "Cant't create card with that number";
    }
}

Card *Card::createCard(int cardNum, const QString &color)
{
    if(Card::isValid(cardNum, Card::validRegularNumbers))
    {
        RegularCard::CARD_COLOR validColor;
        if(color == "pink")
        {
            m_color = "pink";
            validColor = RegularCard::CARD_COLOR::PINK;

        }else if(color == "blue")
        {
            m_color = "blue";
            validColor = RegularCard::CARD_COLOR::BLUE;

        }else
            throw "Invalid color";
        return new RegularCard(cardNum, validColor);

    }else if(Card::isValid(cardNum, Card::validMagicNumbers))
    {
        return new MagicCard(cardNum);
    }else{
        throw "Cant't create card with that number";
    }
}

Card *Card::createCard(Card *card)
{
    return Card::createCard(card->cardNum(), card->color());
}

Card *Card::createCard(const QString& cardStr)
{
    QList<QString> list = cardStr.split(":");
    if (list.size() < 3)
    {
        throw "Invalid card string";
    }
    QString magicOrColor = list[1].trimmed();
    QString numStr = list[2].trimmed();
    bool ok;
    int num=numStr.toInt(&ok);
    if (!ok)
    {
        throw "Failed to convert string to int.";
    } else
    {
        return createCard(num, magicOrColor);
    }

}

void Card::switchColor()
{
    if(Card::m_color == "pink")
        Card::m_color = "blue";
    else if(Card::m_color == "blue")
        Card::m_color = "pink";
    else
        throw "Invalid color";
}

auto operator<<(std::ostream &os, const Card &card) -> std::ostream &
{
    os << card.cardNum();
    return os;
}
