#ifndef DECK_H
#define DECK_H

#include<iostream>
#include "card.h"
#include "pile.h"
#include <QStack>
#include <QVector>
#include <random>
#include <QRandomGenerator>

class Deck
{
public:
    Deck();


    ~Deck();


    bool isEmpty();
    size_t deckSize();

    Card* getFirstCard();

    Card* peekFirstCard() const;

    void shuffle();

    void refill(Pile *pile);

    QVector<Card *> dealHand();

    //koriscenje radi provere
    //friend std::ostream &operator<<(std::ostream &out, Deck &k);

private:
    QStack<Card *> m_deckOfCards;
    void fromStackToVector(QVector<Card*> &vectorOfCards);
    void fromVectorToStack(QVector<Card*> &vectorOfCards);
};

#endif // DECK_H
