#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QRegularExpressionValidator>
#include <QMediaPlayer>
#include <QtMultimedia>
#include <QAudioOutput>
#include <QSoundEffect>

#include "lobby.h"
#include "rules.h"
#include "client.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    //button slots
    void onBtnExitTheApplicationClicked();
    void onTbtnSoundClicked();
    void onBtnPlayTheGameClicked();
    void onTbtnBackSignInClicked();
    void onBtnJoinTheGameClicked();
    void onBtnRulesOfTheGameClicked();
    void onBtnClearLineEditClicked();
    void onTbtnBackResultsClicked();
    void onBtnResultsClicked();

    //avatars
    void onTbtnFirstAvatarClicked();
    void onTbtnSecondAvatarClicked();
    void onTbtnThirdAvatarClicked();
    void onTbtnFourthAvatarClicked();
    void onTbtnFifthAvatarClicked();
    void onTbtnSixthAvatarClicked();
    void onTbtnSeventhAvatarClicked();
    void onTbtnEighthAvatarClicked();

    //music slots
    void onTbtnPlayClicked();
    void onTbtnPauseClicked();
    void onSliderProgressSliderMoved(int position);
    void onSliderVolumeSliderMoved(int position);
    void onDurationChanged(int position);
    void onPositionChanged(int position);
    void onTbtnBackSoundSettingsClicked();

private:
    Ui::MainWindow *ui;
    Client *m_client;
    QString m_username;

    int m_avatarNum = 1;

    void avatarReset();
    void addSoundEffect();
    QString resultsPath();

    QMediaPlayer *m_mediaPlayer;
    QAudioOutput *m_audioOutput;

    QSoundEffect *m_soundEffect;
};
#endif // MAINWINDOW_H
