#ifndef CLIENTGAMESTATE_H
#define CLIENTGAMESTATE_H

#include <QObject>
#include <QDebug>
#include "card.h"
#include "magiccard.h"
#include "regularcard.h"
#include "../movetype.h"
#include "../utilities.h"

class ClientGameState : public QObject
{
    Q_OBJECT
public:
    explicit ClientGameState(const QVector<QString> &playersNames, const QVector<int> &playersAvatars, int myIndex, bool isMyTurn, const QString &pileTop);

    //getters
    QVector<QString> namesOfPlayers();
    QVector<int> numOfCardsOfEachPlayer();
    QVector<int> avatarsOfPlayers();
    int indexOfCurrentPlayer();
    int numOfPlayers();
    bool readyToStart();
    int turnNumber();
    int myIndex();
    bool isMyTurn();
    bool isHoldingCardInHand(); //drzim kartu u ruci trenutno
    bool isDeckCard(); //kliknuo sam na deck
    bool isPileCard(); //kliknuo sam na pile
    bool isMagic();
    bool hasPlayed();
    bool isFirstCardChoosen() const;
    int choosenPlayerIndex1() const;
    int choosenCardIndex1() const;
    int choosenPlayerIndex2() const;
    int choosenCardIndex2() const;
    bool flyInto() const;
    int indexOfPlayerWhoFlyInto() const;
    bool sayCambio() const;
    bool isSomeoneSaidCambio() const;
    int indexOfPlayerWhoSaidCambio() const;
    QString cardInMyHand() const;
    QString pileCard() const;

    //setters
    void setNamesOfPlayers(const QVector<QString> &newNamesOfPlayers);
    void setNumOfCardsOfEachPlayer(const QVector<int> &newNumOfCardsOfEachPlayer);
    void setAvatarsOfPlayers(const QVector<int> &newAvatarsOfPlayers);
    void setIndexOfCurrentPlayer(int newIndexOfCurrentPlayer);
    void setNumOfPlayers(int newNumOfPlayers);
    void setReadyToStart(bool newReadyToStart);
    void setTurnNumber(int newTurnNumber);
    void setMyIndex(int newMyIndex);
    void setIsMyTurn(bool newIsMyTurn);
    void setIsHoldingCardInHand(bool newIsHoldingCardInHand);
    void setIsDeckCard(bool newIsDeckCard);
    void setIsPileCard(bool newIsPileCard);
    void setIsMagic(bool newIsMagic);
    void setHasPlayed(bool newHasPlayed);
    void setIsFirstCardChoosen(bool newIsFirstCardChoosen);
    void setChoosenPlayerIndex1(int newChoosenPlayerIndex1);
    void setChoosenCardIndex1(int newChoosenCardIndex1);
    void setChoosenPlayerIndex2(int newChoosenPlayerIndex2);
    void setChoosenCardIndex2(int newChoosenCardIndex2);
    void setFlyInto(bool newFlyInto);
    void setIndexOfPlayerWhoFlyInto(int newIndexOfPlayerWhoFlyInto);
    void setSayCambio(bool newSayCambio);
    void setIsSomeoneSaidCambio(bool newIsSomeoneSaidCambio);
    void setIndexOfPlayerWhoSaidCambio(int newIndexOfPlayerWhoSaidCambio);
    void setCardInMyHand(const QString &newCardInMyHand);
    void setPileCard(const QString &newPileCard);


    void incrementPlayerNumOfCards(int indexOfPlayer);
    void decrementPlayerNumOfCards(int indexOfPlayer);
    int numOfCardsOfPlayer(int indexOfPlayer);
    void stateHasChanged(MOVE_TYPE);
    void stateFlyInto(bool success, MOVE_TYPE move = MOVE_TYPE::FLY_INTO);

    void setMyTwoCards(const QVector<QString> &newMyTwoCards);

    QVector<QString> myTwoCards() const;


    void setIsIndexOfCurrentPlayerHasChanged(bool newIsIndexOfCurrentPlayerHasChanged);

    bool isIndexOfCurrentPlayerHasChanged() const;

    bool getEndSuccesFlyInto() const;

    void setEndSuccesFlyInto(bool newEndSuccesFlyInto);

    void setFlyIntoSuccessCard(const QString &newFlyIntoSuccessCard);

    QString flyIntoSuccessCard() const;

    QVector<bool> flyIntoVector() const;
    void incrementNumOfFlyInto();
    void decrementNumOfFlyInto();
    int numOfFlyInto() const;

    bool isSomeoneFlyInto() const;

    void setHandOverCardIndex(int newHandOverCardIndex);

    int handOverCardIndex() const;

    void setFlyIntoFailurePlayer(int newFlyIntoFailurePlayer);

    int flyIntoFailureOwner() const;
    void setFlyIntoFailureOwner(int newFlyIntoFailureOwner);

    int flyIntoFailurePlayer() const;

    int flyIntoFailureCardIndex() const;
    void setFlyIntoFailureCardIndex(int newFlyIntoFailureCardIndex);

    QString flyIntoFailureCard() const;
    void setFlyIntoFailureCard(const QString &newFlyIntoFailureCard);

    int indexOfPenaltyCard() const;
    void setIndexOfPenaltyCard(int newIndexOfPenaltyCard);

    int flyIntoIndexOfPlayerWhoseCardIsTaken() const;
    void setFlyIntoIndexOfPlayerWhoseCardIsTaken(int newFlyIntoIndexOfPlayerWhoseCardIsTaken);

    int flyIntoIndexOfTakenCard() const;
    void setFlyIntoIndexOfTakenCard(int newFlyIntoIndexOfTakenCard);

    //TODO:
    void restartFlyInto();
signals:
    void signalMoveType(MOVE_TYPE);
    void signalCurrentPlayerChanged();
    void signalRenderCardOnPile();
    void signalRenderCardOnDeck();
    void signalPileCardChanged();
    void signalMagicCardUsedRender(const QString& magic);
    void signalGameIsReady();
    void signalMeSwap();
    void signalReady();
    void signalResetState();
    void signalSwapCards();
    void signalEndGame(const QVector<QPair<QString, int>> result, const QMap<int, QVector<QString>> playersCards, const int winnersIndex);
    void signalFlyInto(int indexOfPlayerWhoFlyInto, int indexOfPlayerWhoseCardIsTaken, int indexOfCard, const QString &card, bool isSuccess, int indexOfPenaltyCard =-1);
    void signalGiveCard(int indexOfPlayer, int indexOfCard, bool isExtraCard);
    void signalReturnToMainMenu();

private slots:


public slots:
    void onPlayersCardClicked(int indexOfPlayer, int i, int j);
    void onPlayersCardClicked(int indexOfPlayer, int indexOfCard);
    void onBtnMoveClicked(QString move);
    void onDeckClicked();
    void onPileClicked();
    void onPlayerSaidCambio();
    void onPlayersCardDoubleClicked(int indexOfPlayer, int i, int j);
    void onAvatarClicked(int indexOfPlayer);

private:
    //da li karte da budu stringovi ili Card*?
    QVector<QString> m_namesOfPlayers;
    QVector<int> m_numOfCardsOfEachPlayer;
    QVector<int> m_avatarsOfPlayers;
    QVector<QString> m_myTwoCards;

    //MY INNER STATE

    //ovo mi govori da se zavrsio potez i ako sam ja bio na potezu, resetujem game state
    bool m_isIndexOfCurrentPlayerHasChanged; //ovo podesva klijent,ja gledam da li je true
    int m_indexOfCurrentPlayer; //ne diram, podesava client
    int m_numOfPlayers; // ovo mora da bude biti konst

    bool m_readyToStart; //ovo popunjava klijent, ja samo proveravam da li je true, ako jeste mozemo da igramo
    int m_turnNumber; //koji je broj poteza, zbog slanja poruka serveru



    int m_myIndex;
    bool m_isMyTurn; //ja ovo proveravam i podesavam! kada se promeni currentPlayer i kada je indexTrenutnogIgraca == momIndexu, ovo postaje true
    bool m_isHoldingCardInHand; // promenljiva koja kaze da li neko drugi drzi kartu u ruci//drzim kartu u ruci trenutno
    bool m_isDeckCard; //kliknuo sam na deck, ovo ja podesavam, ovo treba da restartujem na kraju mog poteza
    bool m_isPileCard; //kliknuo sam na pile, ovo ja podesavam, ovo treba da restartujem na kraju mog poteza
    bool m_isMagic; //ovo postavlja client, a ja resetujem. imam potencijal da iskoristim magiju(vukao sam sa decka magicnu)
    bool m_hasPlayed; // ovo ja proveravam,podesavam i resetujem
    bool m_isMeSwapState;

    //stringovna reprezentacija karte
    QString m_pileCard;
    QString m_cardInMyHand; // karta koju drzim u ruci

    bool m_isFirstCardChoosen; // ovo client postavlja, ja resetujem
    int m_choosenPlayerIndex1; // ovo client postavlja, ja resetujem
    int m_choosenCardIndex1;   // ovo client postavlja, ja resetujem
    int m_choosenPlayerIndex2; // ovo client postavlja, ja resetujem
    int m_choosenCardIndex2;   // ovo client postavlja, ja resetujem

    bool m_flyInto;
    //bool m_whoTryToFlyInto;
    bool m_endSuccesFlyInto; //??
    int m_indexOfPlayerWhoFlyInto; //indeks onoga koji je uspesno uleteo
    QString m_flyIntoSuccessCard; //karta kojom je izvrseno uspeno uletanje
    int m_flyIntoIndexOfPlayerWhoseCardIsTaken; //koordinate karte kojom se uspesno uletelo
    int m_flyIntoIndexOfTakenCard;
    int m_handOverCardIndex; //koordinate karte koju zelim da predam
    int m_numOfFlyInto; //niz uletanja bilo uspesna bilo neuspesna
                                    // tako ces znati kada su sva uletanja zavrsena da mozes da resetujes stanje
    int m_flyIntoFailurePlayer;
    int m_flyIntoFailureOwner;
    int m_flyIntoFailureCardIndex;
    QString m_flyIntoFailureCard;
    int m_indexOfPenaltyCard;

    bool m_sayCambio; // onButtonCambioClicked
    bool m_isSomeoneSaidCambio; //???
    int m_indexOfPlayerWhoSaidCambio; // ovo podesava client

    Utilities m_help;
    //methods...
    void restartMyState();
    int previousPlayer();


};

#endif // CLIENTGAMESTATE_H
