#include "gamegui.h"
#include "ui_gamegui.h"

#include <QScreen>
#include <QHBoxLayout>
#include <QFont>
#include <QGridLayout>
#include <QLayoutItem>
#include <QWidget>

GameGui::GameGui(ClientGameState *clientGameState, QWidget *parent) :
    QWidget(parent),
    m_clientGameState(clientGameState),
    ui(new Ui::GameGui)
{
    ui->setupUi(this);
    this->showFullScreen();
    this->setWindowTitle("C++AMBIO");

    m_help = Utilities(m_clientGameState->myIndex(), m_clientGameState->numOfPlayers());
    initializeMainPlayerFrame();
    initializeOpponentFrames();
    renderTable();

    m_mediaPlayer = new QMediaPlayer();
    m_unsuccessfulSound = new QMediaPlayer();
    m_audioOutput = new QAudioOutput();
    m_mediaPlayer->setAudioOutput(m_audioOutput);
    m_unsuccessfulSound->setAudioOutput(new QAudioOutput());
    m_unsuccessfulSound->setSource(QUrl::fromUserInput("qrc:/Sounds/UnsuccessfulFlyIntoSound.mp3"));
    m_mediaPlayer->setSource(QUrl::fromUserInput("qrc:/Sounds/gameRoomSound.mp3"));
    m_mediaPlayer->play();

    connectButtons();
    connectSignals();
}

GameGui::~GameGui()
{
    delete ui;
    delete m_audioOutput;
    delete m_mediaPlayer;
}

void GameGui::connectButtons()
{
    QObject::connect(ui->btnDeck, &QPushButton::clicked, m_clientGameState,
                     &ClientGameState::onDeckClicked);
    QObject::connect(ui->btnPile, &QPushButton::clicked, m_clientGameState,
                     &ClientGameState::onPileClicked);
    QObject::connect(ui->btnCambio, &QPushButton::clicked, this,
                     &GameGui::onBtnCambioClicked);
    QObject::connect(ui->btnBack, &QPushButton::clicked, this,
                     &GameGui::onBtnBackClicked);
    QObject::connect(ui->btnCambio, &QPushButton::clicked, m_clientGameState,
                     &ClientGameState::onPlayerSaidCambio);
    QObject::connect(ui->tbtnGameRoomSound, &QToolButton::clicked, this,
                     &GameGui::onTbtnGameRoomSoundClicked);
    QObject::connect(ui->tbtnGameGuide, &QToolButton::clicked, this,
                     &GameGui::onTbtnGameGuideClicked);
    QObject::connect(ui->btnMove, &QPushButton::clicked, this,
                     [this]{m_clientGameState->onBtnMoveClicked(ui->btnMove->text());});
    QObject::connect(m_mediaPlayer, &QMediaPlayer::mediaStatusChanged, m_mediaPlayer,
                     &QMediaPlayer::play);
}

void GameGui::connectSignals()
{
    QObject::connect(m_clientGameState, &ClientGameState::signalCurrentPlayerChanged, this,
                     &GameGui::onCurrentPlayerChanged);
    QObject::connect(m_clientGameState, &ClientGameState::signalRenderCardOnDeck, this,
                     &GameGui::onRenderCardOnDeck);
    QObject::connect(m_clientGameState, &ClientGameState::signalRenderCardOnPile, this,
                     &GameGui::onRenderCardOnPile);
    QObject::connect(m_clientGameState, &ClientGameState::signalPileCardChanged, this,
                     &GameGui::onPileCardChanged);
    QObject::connect(m_clientGameState, &ClientGameState::signalMagicCardUsedRender, this,
                     &GameGui::onMagicCardUsedRender);
    QObject::connect(m_clientGameState, &ClientGameState::signalGameIsReady, this,
                     &GameGui::onBtnReadyClicked);
    QObject::connect(m_clientGameState, &ClientGameState::signalResetState, this,
                     &GameGui::onResetState);
    QObject::connect(m_clientGameState, &ClientGameState::signalMeSwap, this,
                     &GameGui::onMeSwap);
    QObject::connect(m_clientGameState, &ClientGameState::signalSwapCards, this,
                     &GameGui::onSwapCards);
    QObject::connect(m_clientGameState, &ClientGameState::signalEndGame, this,
                     &GameGui::onEndOfGame);
    QObject::connect(m_clientGameState, &ClientGameState::signalFlyInto, this,
                     &GameGui::onFlyInto);
    QObject::connect(m_clientGameState, &ClientGameState::signalGiveCard, this,
                     &GameGui::onGiveCard);

}

void GameGui::renderCardsInHand()
{
    auto *layout = qobject_cast<QGridLayout *>(m_frames.at(0)->layout());

    while (layout->count())
    {
        QLayoutItem *item = layout->takeAt(0);
        delete item->widget();
        delete item;
    }

    int readyBtnWidth = 180;
    int readyBtnHeight = 51;
    ui->btnMove->setFixedSize(readyBtnWidth, readyBtnHeight);
    ui->btnMove->setStyleSheet(
        "QPushButton {background-color: rgb(94, 92, 100);"
        "color: rgb(255, 255, 255);"
        "font: 15pt \"Ubuntu Condensed\";"
        " border-radius: 10px;}"
        "QPushButton:hover {background-color: rgba(154, 153, 150, 70%);"
        "color: rgb(255, 255, 255);"
        "font: 15pt \"Ubuntu Condensed\";"
        " border-radius: 10px;}"
        "QPushButton:press {background-color: rgb(94, 92, 100);"
        "color: rgb(255, 255, 255);"
        "font: 15pt \"Ubuntu Condensed\";"
        " border-radius: 10px;}"
        );
    ui->btnMove->setGeometry(m_screenWidth / 2 - readyBtnWidth / 2, m_screenHeight / 2 + 10, readyBtnWidth,
                              readyBtnHeight);

    QString myFirstCard = m_clientGameState->myTwoCards().at(0);
    QString mySecondCard = m_clientGameState->myTwoCards().at(1);

    int numOfCards = m_clientGameState->numOfCardsOfEachPlayer().at(m_clientGameState->myIndex());
    for(int i=0; i < 2; i++){
        for(int j=0; j < (numOfCards % 2 == 0 ? numOfCards / 2 : numOfCards / 2 + 1); j++)
        {
            auto *card = new DoubleClickButton("", m_frames.at(0));
            card->setFixedSize(60, 95);
            //TODO: treba da se pokupi indeks igraca i karte, za sad je hardkodirano
            int playerIndex = m_clientGameState->myIndex();

            QObject::connect(card, &DoubleClickButton::doubleClicked, this,
                             [this, playerIndex, i, j](){ m_clientGameState->onPlayersCardDoubleClicked(playerIndex, i, j); });

            QObject::connect(card, &DoubleClickButton::singleClicked, this,
                             [this, playerIndex, i, j](){ m_clientGameState->onPlayersCardClicked(playerIndex, i, j); });


            if(i == 1 && j == 0)
            {
                QString ss = QStringLiteral("QPushButton {border-image: url(%1); border-radius: 10px;}").arg(cardPath(myFirstCard));
                card->setStyleSheet(ss);
            }else if(i == 1 && j == 1)
            {
                QString ss = QStringLiteral("QPushButton {border-image: url(%1); border-radius: 10px;}").arg(cardPath(mySecondCard));
                card->setStyleSheet(ss);
            }
            else
            {
                card->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBack.png); border-radius: 10px;}");
            }
            layout->addWidget(card, i, j, Qt::AlignCenter);
        }
    }
}

void GameGui::renderPlayerCard(int indexOfPlayer, int indexOfCard, const QString &card)
{
    QString path = "";

    if(card == "Back")
    {
        path = ":/Cards/Back/cardBack.png";
    }else
    {
        path = cardPath(card);
    }
    int ind = -1;

    for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
    {
        if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == indexOfPlayer)
        {
            ind = i;
            break;
        }
    }

    if(ind != -1)
    {
        auto *layout = qobject_cast<QGridLayout *>(m_frames.at(ind)->layout());
        QPair<int, int> index = m_help.expandCardIndex(indexOfPlayer, indexOfCard);
        QLayoutItem *item = layout->itemAtPosition(index.first, index.second);

        QPushButton *btnCard = nullptr;

        if(item && item->widget())
        {
            btnCard = qobject_cast<QPushButton *>(item->widget());

            QString ss = QStringLiteral("QPushButton {border-image: url(%1); border-radius: 10px; background-color:transparent;}").arg(path);
            btnCard->setStyleSheet(ss);
            btnCard->show();
        }
    }
}

void GameGui::successfulFlyInto(int indexOfPlayerWhoFlyInto, int indexOfPlayerWhoseCardIsTaken, int indexOfCard, const QString &card, bool endFlyInto)
{
    onRenderCardOnPile();

    hidePlayerCard(indexOfPlayerWhoseCardIsTaken, indexOfCard);

    if(endFlyInto)
    {
        m_clientGameState->decrementNumOfFlyInto();
    }


}

void GameGui::unsuccessfulFlyInto(int indexOfPlayerWhoFlyInto, int indexOfPlayerWhoseCardIsTaken, int indexOfCard, const QString &card, int indexOfPenaltyCard)
{
    m_mediaPlayer->play();
    m_unsuccessfulSound->stop();
    renderPlayerCard(indexOfPlayerWhoseCardIsTaken, indexOfCard, card);

    bool isExtraCard = indexOfPenaltyCard == 4;
    onGiveCard(indexOfPlayerWhoFlyInto, indexOfPenaltyCard, isExtraCard);

    m_clientGameState->decrementNumOfFlyInto();
}

void GameGui::hidePlayerCard(int indexOfPlayer, int indexOfCard)
{
    int ind = -1;

    for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
    {
        if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == indexOfPlayer)
        {
            ind = i;
            break;
        }
    }

    if(ind != -1)
    {
        auto *layout = qobject_cast<QGridLayout *>(m_frames.at(ind)->layout());
        QPair<int, int> index = m_help.expandCardIndex(indexOfPlayer,indexOfCard);
        QLayoutItem *item = layout->itemAtPosition(index.first, index.second);

        QPushButton *btnCard = nullptr;

        if(item && item->widget())
        {
            btnCard = qobject_cast<QPushButton *>(item->widget());
            btnCard->hide();
        }
    }
}

void GameGui::onGiveCard(int indexOfPlayer, int indexOfCard, bool isExtraCard)
{
    int ind = -1;

    for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
    {
        if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == indexOfPlayer)
        {
            ind = i;
            break;
        }
    }

    QGridLayout *layout;

    if(ind != -1)
    {
        layout = qobject_cast<QGridLayout *>(m_frames.at(ind)->layout());
    }

    QPair<int, int> index = m_help.expandCardIndex(indexOfPlayer, indexOfCard);
    int i = index.first;
    int j = index.second;

    if(isExtraCard)
    {
        auto *frame = qobject_cast<QFrame *>(m_frames.at(ind));
        int frameX = frame->geometry().x();
        int frameY = frame->geometry().y();
        int frameWidth = frame->width();
        int frameHeight = frame->height();

        auto *indicator = qobject_cast<QPushButton *>(m_indicators.at(ind));
        int indicatorX = indicator->geometry().x();
        int indicatorY = indicator->geometry().y();
        int indicatorSize = indicator->width();
        if(ind == 3)
        {
            frame->setGeometry(frameX - 70, frameY, frameWidth + 70, frameHeight);
            indicator->setGeometry(indicatorX - 50, indicatorY, indicatorSize, indicatorSize);
        }
        else
        {
            frame->setGeometry(frameX, frameY, frameWidth + 70, frameHeight);
            indicator->setGeometry(indicatorX + 50, indicatorY, indicatorSize, indicatorSize);
        }
        auto *card = new DoubleClickButton("", m_frames.at(ind));
        card->setFixedSize(60, 95);

        QObject::connect(card, &DoubleClickButton::doubleClicked, this,
                         [this, indexOfPlayer, i, j](){ m_clientGameState->onPlayersCardDoubleClicked(indexOfPlayer, i, j); });

        QObject::connect(card, &DoubleClickButton::singleClicked, this,
                         [this, indexOfPlayer, i, j](){ m_clientGameState->onPlayersCardClicked(indexOfPlayer, i, j); });

        card->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBack.png); border-radius: 10px;}");

        layout->addWidget(card, i, j, Qt::AlignCenter);
    }else
    {
        renderPlayerCard(indexOfPlayer, indexOfCard, "Back");
    }
}

void GameGui::onBtnReadyClicked()
{
    auto *layout = qobject_cast<QGridLayout *>(m_frames.at(0)->layout());
    QLayoutItem *item1 = layout->itemAtPosition(1, 0);
    QLayoutItem *item2 = layout->itemAtPosition(1, 1);

    QPushButton *btnCard1 = nullptr;
    QPushButton *btnCard2 = nullptr;

    if(item1 && item1->widget())
    {
        btnCard1 = qobject_cast<QPushButton *>(item1->widget());
        btnCard1->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBack.png); border-radius: 10px; background-color:transparent;}");
    }

    if(item2 && item2->widget())
    {
        btnCard2 = qobject_cast<QPushButton *>(item2->widget());
        btnCard2->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBack.png); border-radius: 10px; background-color:transparent;}");
    }

    ui->btnMove->hide();

}

void GameGui::onBtnCambioClicked()
{
    ui->btnCambio->setStyleSheet("background-color: rgba(154, 153, 150, 70%); color: rgba(255, 255, 255, 70%); font: 20pt \"Ubuntu Condensed\"; border-radius: 10px;");
    ui->btnCambio->setDisabled(true);
}

void GameGui::onPileCardChanged()
{
    QString card = m_clientGameState->pileCard();
    QString path = cardPath(card);

    ui->btnPile->setStyleSheet("QPushButton {border-image: url(" + path + "); border-radius: 15px; background-color:transparent;}");

    if(m_clientGameState->isMyTurn())
    {
        ui->btnDeck->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBack.png); border-radius: 15px; background-color:transparent;}");

        if(m_clientGameState->isMagic())
        {
            ui->btnMove->setText("Preskoči");
            ui->btnMove->show();
        }
    }
}

void GameGui::onMagicCardUsedRender(const QString magic)
{
    QString card = m_clientGameState->cardInMyHand();
    QString path = cardPath(card);

    int ind = -1;

    for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
    {
        if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == m_clientGameState->choosenPlayerIndex1())
        {
            ind = i;
            break;
        }
    }

    if(ind != -1)
    {
        auto *layout = qobject_cast<QGridLayout *>(m_frames.at(ind)->layout());
        QPair<int, int> index = m_help.expandCardIndex(m_clientGameState->choosenPlayerIndex1(),m_clientGameState->choosenCardIndex1());
        QLayoutItem *item = layout->itemAtPosition(index.first, index.second);

        QPushButton *btnCard = nullptr;


        if(item && item->widget())
        {
            btnCard = qobject_cast<QPushButton *>(item->widget());

            if(m_clientGameState->isMyTurn())
            {
                if(magic == "Eye" || magic == "Detective")
                {
                    ui->btnMove->setText("Gotov");
                    ui->btnMove->show();
                    btnCard->setStyleSheet("QPushButton {border-image: url(" + path + "); border-radius: 10px; background-color:transparent;}");
                }
                if(magic == "LookAndSwap")
                {
                    btnCard->setStyleSheet("QPushButton {border-image: url(" + path + "); border-radius: 10px; background-color:transparent;}");
                }
                if(magic == "Swap")
                {
                    ui->btnMove->hide();
                    QString ss = QStringLiteral("QPushButton {border-image: url(:/Cards/Back/cardBack%1.png); border-radius: 10px; background-color:transparent;}").arg(magic);
                    btnCard->setStyleSheet(ss);
                }
            }
            else
            {
                ui->btnMove->hide();

                QString ss = QStringLiteral("QPushButton {border-image: url(:/Cards/Back/cardBack%1.png); border-radius: 10px; background-color:transparent;}").arg(magic);
                btnCard->setStyleSheet(ss);
            }
        }
    }
}

void GameGui::onEndOfGame(const QVector<QPair<QString, int>> result, const QMap<int, QVector<QString>> playersCards, const int winnersIndex)
{
    int ind = -1;

    for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
    {
        if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == winnersIndex)
        {
            ind = i;
            break;
        }
    }
    if(ind != -1)
    {
        m_avatars.at(ind)->setStyleSheet("QPushButton {border-image: url(:/Icons/trophy.png); background-color: transparent;}");
        m_avatars.at(ind)->show();
    }

    ui->btnCambio->hide();
    ui->btnDeck->hide();
    ui->btnPile->hide();

    foreach (int indexOfPlayer, playersCards.keys()) {

        const QVector<QString> &cards = playersCards.value(indexOfPlayer);

        for(int indexOfCard = 0; indexOfCard < cards.size(); indexOfCard++)
        {
            renderPlayerCard(indexOfPlayer, indexOfCard, cards[indexOfCard]);
        }
    }

    QLabel *lblResaults = new QLabel(this);

    QString labelText;
    int i = 1;
    for (const auto& pair : result) {
        labelText += QString::number(i) + ".\t" + pair.first + "\t" + QString::number(pair.second) + "\n";
        i++;
    }

    if(m_clientGameState->myIndex() == winnersIndex)
    {
        lblResaults->setStyleSheet("background-color: transparent; border: black; border-image: url(:/Icons/winner.png);");
    }
    else
    {
        lblResaults->setStyleSheet("background-color: transparent;");
    }

    lblResaults->setText(labelText);
    lblResaults->setFont(QFont("Ubuntu condensed", 15, QFont::Bold));
    int lblResaultsWidth = 150;
    int lblresaultsHeight = 150;
    lblResaults->setAlignment(Qt::AlignCenter);
    lblResaults->setFrameStyle(QFrame::Box | QFrame::Plain);
    lblResaults->setGeometry(m_screenWidth / 2 - lblResaultsWidth / 2, m_screenHeight / 2 - lblresaultsHeight / 2 - 5, lblResaultsWidth, lblresaultsHeight);
    lblResaults->show();

    ui->btnBack->setStyleSheet(
        "QPushButton {background-color: rgb(94, 92, 100);"
        "color: rgb(255, 255, 255);"
        "font: 15pt \"Ubuntu Condensed\";"
        " border-radius: 10px;}"
        "QPushButton:hover {background-color: rgba(154, 153, 150, 70%);"
        "color: rgb(255, 255, 255);"
        "font: 15pt \"Ubuntu Condensed\";"
        " border-radius: 10px;}"
        "QPushButton:press {background-color: rgb(94, 92, 100);"
        "color: rgb(255, 255, 255);"
        "font: 15pt \"Ubuntu Condensed\";"
        " border-radius: 10px;}"
        );

    int backBtnWidth = 200;
    int backBtnHeight = 30;
    ui->btnBack->setGeometry(m_screenWidth / 2 - backBtnWidth / 2, m_screenHeight / 2 + backBtnHeight + lblresaultsHeight / 2, backBtnWidth, backBtnHeight);
    ui->btnBack->setDisabled(false);
    ui->btnBack->setText("Povratak u glavni meni");
    ui->btnBack->show();
}

void GameGui::onBtnBackClicked()
{
    delete m_audioOutput;
    delete m_mediaPlayer;
    delete m_unsuccessfulSound;

    MainWindow *mainWindow = new MainWindow();
    mainWindow->show();
    emit m_clientGameState->signalReturnToMainMenu();
    this->close();

}

void GameGui::onFlyInto(int indexOfPlayerWhoFlyInto, int indexOfPlayerWhoseCardIsTaken, int indexOfCard, QString card, bool isSuccess, int indexOfPenaltyCard)
{
    if(card!="")
    {
        renderPlayerCard(indexOfPlayerWhoseCardIsTaken, indexOfCard, card);
        if(isSuccess)
        {
            bool endFlyInto = indexOfPlayerWhoFlyInto == indexOfPlayerWhoseCardIsTaken;
            QTimer::singleShot(2000, this, [this, indexOfPlayerWhoFlyInto, indexOfPlayerWhoseCardIsTaken, indexOfCard, card, endFlyInto ](){ successfulFlyInto(indexOfPlayerWhoFlyInto, indexOfPlayerWhoseCardIsTaken, indexOfCard, card, endFlyInto);});
        }else
        {
            m_mediaPlayer->pause();
            m_unsuccessfulSound->play();
            QTimer::singleShot(3000, this, [this, indexOfPlayerWhoFlyInto, indexOfPlayerWhoseCardIsTaken, indexOfCard, indexOfPenaltyCard](){ unsuccessfulFlyInto(indexOfPlayerWhoFlyInto, indexOfPlayerWhoseCardIsTaken, indexOfCard, "Back", indexOfPenaltyCard);});
        }
    }else
    {
        //na indexOfPlayerWhoFlyInto na polje handOvercard treba da se hide
        //treba da se prikaze na indexOfPlayerWhoseCardIsTaken na indexofCard
        hidePlayerCard(indexOfPlayerWhoFlyInto, indexOfPenaltyCard);

        bool isExtraCard = indexOfCard == 4;
        onGiveCard(indexOfPlayerWhoseCardIsTaken, indexOfCard, isExtraCard);
        m_clientGameState->decrementNumOfFlyInto();
    }

}

void GameGui::onMeSwap()
{
    int ind = -1;
    for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
    {
        if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == m_clientGameState->choosenPlayerIndex1())
        {
            ind = i;
            break;
        }
    }

    if(ind != -1)
    {
        auto *layout = qobject_cast<QGridLayout *>(m_frames.at(ind)->layout());
        QPair<int, int> index = m_help.expandCardIndex(m_clientGameState->choosenPlayerIndex1(),m_clientGameState->choosenCardIndex1());
        QLayoutItem *item = layout->itemAtPosition(index.first, index.second);

        QPushButton *btnCard = nullptr;

        if(item && item->widget())
        {
            btnCard = qobject_cast<QPushButton *>(item->widget());
            btnCard->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBackClick.png); border-radius: 10px; background-color:transparent;}");
        }
    }
}

void GameGui::onSwapCards()
{
    if(m_clientGameState->choosenPlayerIndex1() == -1 || m_clientGameState->choosenPlayerIndex2() == -1
        || m_clientGameState->choosenCardIndex1() == -1 || m_clientGameState->choosenCardIndex2() == -1)
    {
        throw "Indeksi nisu dobri";
    }

    int ind1 = -1;
    int ind2 = -1;

    for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
    {
        if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == m_clientGameState->choosenPlayerIndex1())
        {
            ind1 = i;
            break;
        }
    }

    for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
    {
        if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == m_clientGameState->choosenPlayerIndex2())
        {
            ind2 = i;
            break;
        }
    }

    if(m_clientGameState->isMyTurn())
    {
        ui->btnMove->setText("Gotov");
        ui->btnMove->show();
    }


    if(ind1 != -1)
    {
        auto *layout1 = qobject_cast<QGridLayout *>(m_frames.at(ind1)->layout());
        QPair<int, int> index1 = m_help.expandCardIndex(m_clientGameState->choosenPlayerIndex1(),m_clientGameState->choosenCardIndex1());
        QLayoutItem *item1 = layout1->itemAtPosition(index1.first, index1.second);

        QPushButton *btnCard1 = nullptr;

        if(item1 && item1->widget())
        {
            btnCard1 = qobject_cast<QPushButton *>(item1->widget());
            btnCard1->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBackSwap.png); border-radius: 10px; background-color:transparent;}");
        }
    }

    if(ind2 != -1)
    {
        auto *layout2 = qobject_cast<QGridLayout *>(m_frames.at(ind2)->layout());
        QPair<int, int> index2 = m_help.expandCardIndex(m_clientGameState->choosenPlayerIndex2(),m_clientGameState->choosenCardIndex2());
        QLayoutItem *item2 = layout2->itemAtPosition(index2.first, index2.second);

        QPushButton *btnCard2 = nullptr;

        if(item2 && item2->widget())
        {
            btnCard2 = qobject_cast<QPushButton *>(item2->widget());
            btnCard2->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBackSwap.png); border-radius: 10px; background-color:transparent;}");
        }
    }
}


void GameGui::onResetState()
{
    int btnWidth = 100;
    int btnHeight = 150;
    ui->btnMove->hide();
    ui->btnDeck->setGeometry(m_screenWidth / 2 + 110, m_screenHeight / 2 - btnHeight / 2 , btnWidth, btnHeight);
    ui->btnDeck->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBack.png); border-radius: 15px; background-color:transparent;}");

    ui->btnPile->setGeometry(m_screenWidth / 2 - btnWidth - 110, m_screenHeight / 2 - btnHeight / 2 , btnWidth, btnHeight);
    QString card = m_clientGameState->pileCard();
    QString path = cardPath(card);

    ui->btnPile->setStyleSheet("QPushButton {border-image: url(" + path + "); border-radius: 15px; background-color:transparent;}");

    if(m_clientGameState->choosenPlayerIndex1() != -1)
    {
        int ind = -1;
        for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
        {
            if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == m_clientGameState->choosenPlayerIndex1())
            {
                ind = i;
                break;
            }
        }

        if(ind != -1)
        {
            auto *layout = qobject_cast<QGridLayout *>(m_frames.at(ind)->layout());
            QPair<int, int> index = m_help.expandCardIndex(m_clientGameState->choosenPlayerIndex1(),m_clientGameState->choosenCardIndex1());
            QLayoutItem *item = layout->itemAtPosition(index.first, index.second);

            QPushButton *btnCard = nullptr;

            if(item && item->widget())
            {
                btnCard = qobject_cast<QPushButton *>(item->widget());
                btnCard->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBack.png); border-radius: 10px; background-color:transparent;}");
            }
        }
    }

    if(m_clientGameState->choosenPlayerIndex2() != -1)
    {
        int ind = -1;
        for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
        {
            if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == m_clientGameState->choosenPlayerIndex2())
            {
                ind = i;
                break;
            }
        }

        if(ind != -1)
        {
            auto *layout = qobject_cast<QGridLayout *>(m_frames.at(ind)->layout());
            QPair<int, int> index = m_help.expandCardIndex(m_clientGameState->choosenPlayerIndex2(),m_clientGameState->choosenCardIndex2());
            QLayoutItem *item = layout->itemAtPosition(index.first, index.second);

            QPushButton *btnCard = nullptr;

            if(item && item->widget())
            {
                btnCard = qobject_cast<QPushButton *>(item->widget());
                btnCard->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBack.png); border-radius: 10px; background-color:transparent;}");
            }
        }
    }
}


void GameGui::renderTable()
{
    ui->btnBack->hide();
    ui->tbtnGameGuide->setGeometry(m_screenWidth - 40, 10 , 35, 35);
    if(!m_clientGameState->isMyTurn()){
        ui->btnCambio->setStyleSheet("background-color: rgba(154, 153, 150, 70%); color: rgba(255, 255, 255, 70%); font: 20pt \"Ubuntu Condensed\"; border-radius: 10px;");
        ui->btnCambio->setDisabled(true);
    }
    else{
        ui->btnCambio->setStyleSheet("background-color: rgb(94, 92, 100); color: rgb(255, 255, 255); font: 20pt \"Ubuntu Condensed\"; border-radius: 10px;");
        ui->btnCambio->setDisabled(false);

    }
    int cambioBtnWidth = 180;
    int cambioBtnHeight = 51;
    ui->btnCambio->setGeometry(m_screenWidth / 2 - cambioBtnWidth / 2, m_screenHeight / 2 - cambioBtnHeight, cambioBtnWidth, cambioBtnHeight);

    int deckBtnWidth = 100;
    int deckBtnHeight = 150;
    ui->btnDeck->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBack.png); border-radius: 15px;}");
    ui->btnDeck->setGeometry(m_screenWidth / 2 + cambioBtnWidth / 2 + 20, m_screenHeight / 2 - deckBtnHeight / 2 , deckBtnWidth, deckBtnHeight);

    QString card = m_clientGameState->pileCard();
    QString path = cardPath(card);
    int pileBtnWidth = 100;
    int pileBtnHeight = 150;
    ui->btnPile->setStyleSheet("QPushButton {border-image: url(" + path + "); border-radius: 15px;}");
    ui->btnPile->setGeometry(m_screenWidth / 2 - pileBtnWidth - 20 - cambioBtnWidth / 2, m_screenHeight / 2 - pileBtnHeight / 2 , pileBtnWidth, pileBtnHeight);


    renderMainPlayerFrame();
    renderOpponentFrames();
}

void GameGui::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Escape)
    {
        dialog->show(); //MEM CHECK: OVDE PRIJAVLJUJE CURENJE MEMORIJE!
    }
}

QString GameGui::cardPath(const QString &card)
{
    QString type = card.split(":")[0];
    QString path = "";

    if (type == "reg")
    {
        QString color = card.split(":")[1];
        QString num = card.split(":")[2];

        if(color == "pink")
        {
            path = QStringLiteral(":/Cards/Pink/pink%1.png").arg(num);
        }
        else{
            path = QStringLiteral(":/Cards/Blue/blue%1.png").arg(num);
        }
    }

    if (type == "magic")
    {
        QString magicType = card.split(":")[1];

        path = QStringLiteral(":/Cards/Magic/magic%1.png").arg(magicType);

    }

    return path;

}

void GameGui::renderOpponentFrames()
{
    for(int i = 1; i < m_clientGameState->numOfPlayers(); i++)
    {
        auto *layout = qobject_cast<QGridLayout *>(m_frames.at(i)->layout());

        while (layout->count())
        {
            QLayoutItem *item = layout->takeAt(0);
            delete item->widget();
            delete item;
        }

        int opponentIndex = (i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers();

        int numOfOpponentsCards = m_clientGameState->numOfCardsOfEachPlayer().at(opponentIndex);

        for(int j=0; j < 2; j++){
            for(int k=0; k < (numOfOpponentsCards % 2 == 0 ? numOfOpponentsCards / 2 : numOfOpponentsCards / 2 + 1); k++)
            {
                auto *opponentCard = new DoubleClickButton("", m_frames.at(i));
                QObject::connect(opponentCard, &DoubleClickButton::doubleClicked, this,
                                 [this, opponentIndex, j, k](){ m_clientGameState->onPlayersCardDoubleClicked(opponentIndex, j, k); });

                QObject::connect(opponentCard, &DoubleClickButton::singleClicked, this,
                                 [this, opponentIndex, j, k](){ m_clientGameState->onPlayersCardClicked(opponentIndex, j, k); });

                opponentCard->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBack.png); border-radius: 10px;}");
                opponentCard->setFixedSize(60, 95);
                layout->addWidget(opponentCard, j, k, Qt::AlignCenter);
            }
        }

        m_frames.at(i)->show();

        if(opponentIndex != m_clientGameState->indexOfCurrentPlayer())
        {
            m_indicators.at(i)->setStyleSheet("QPushButton {border-image: url(:/Icons/off.png); background-color: transparent;}");
        }else{
            m_indicators.at(i)->setStyleSheet("QPushButton {border-image: url(:/Icons/on.png); background-color: transparent;}");
        }
        m_indicators.at(i)->show();

        m_names.at(i)->setText(m_clientGameState->namesOfPlayers().at(opponentIndex));
        m_names.at(i)->show();

        QString ss = QStringLiteral("QPushButton {border-image: url(:/Icons/avatar%1.png); background-color: transparent;}").arg(m_clientGameState->avatarsOfPlayers().at(opponentIndex));
        m_avatars.at(i)->setStyleSheet(ss);
        m_avatars.at(i)->show();
    }
}

void GameGui::renderMainPlayerFrame()
{
    renderCardsInHand();
    m_frames.at(0)->show();
    m_indicators.at(0)->show();
    m_names.at(0)->show();
    m_avatars.at(0)->show();
    int index = m_clientGameState->myIndex();
    QObject::connect(m_avatars.at(0), &QPushButton::clicked, this,
                     [this, index](){ m_clientGameState->onAvatarClicked(index); });
}

void GameGui::onRenderCardOnDeck()
{

    if(m_clientGameState->myIndex() == m_clientGameState->indexOfCurrentPlayer())
    {
        QString path = cardPath(m_clientGameState->cardInMyHand());
        QString ss = QStringLiteral("QPushButton {border-image: url(%1); border-radius: 15px; background-color:transparent;}").arg(path);
        ui->btnDeck->setStyleSheet(ss);
    }else
    {
        int deckBtnWidth;
        int deckBtnHeight;
        int deckX, deckY;

        deckBtnWidth = ui->btnDeck->geometry().width();
        deckBtnHeight = ui->btnDeck->geometry().height();

        int dx = 30;
        int dy = 50;

        deckX = ui->btnDeck->geometry().x();
        deckY = ui->btnDeck->geometry().y();

        ui->btnDeck->setGeometry(deckX, deckY - dy / 2, deckBtnWidth + dx, deckBtnHeight + dy);
    }
}

void GameGui::onRenderCardOnPile()
{
    int pileBtnWidth;
    int pileBtnHeight;
    int pileX, pileY;


    QString card = m_clientGameState->pileCard();
    QString path = cardPath(card);

    pileBtnWidth = ui->btnPile->geometry().width();
    pileBtnHeight = ui->btnPile->geometry().height();

    int dx = 30;
    int dy = 50;

    pileX = ui->btnPile->geometry().x();
    pileY = ui->btnPile->geometry().y();

    ui->btnPile->setStyleSheet("QPushButton {border-image: url(" + path + "); border-radius: 15px; background-color:transparent;}");
    ui->btnPile->setGeometry(pileX - dx, pileY - dy / 2, pileBtnWidth + dx, pileBtnHeight + dy);

}

void GameGui::onCurrentPlayerChanged()
{

    if(m_clientGameState->myIndex() == m_clientGameState->indexOfCurrentPlayer())
    {
        if(!m_clientGameState->isSomeoneSaidCambio())
        {
            ui->btnCambio->setStyleSheet("background-color: rgb(94, 92, 100); color: rgb(255, 255, 255); font: 20pt \"Ubuntu Condensed\"; border-radius: 10px;");
            ui->btnCambio->setDisabled(false);

        } else
        {
            ui->btnCambio->setStyleSheet("background-color: rgba(154, 153, 150, 70%); color: rgba(255, 255, 255, 70%); font: 20pt \"Ubuntu Condensed\"; border-radius: 10px;");
            ui->btnCambio->setDisabled(true);
        }
    }
    else {
        ui->btnCambio->setStyleSheet("background-color: rgba(154, 153, 150, 70%); color: rgba(255, 255, 255, 70%); font: 20pt \"Ubuntu Condensed\"; border-radius: 10px;");
        ui->btnCambio->setDisabled(true);
    }

    for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
    {
        if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == m_clientGameState->indexOfCurrentPlayer())
        {
            m_indicators.at(i)->setStyleSheet("QPushButton {border-image: url(:/Icons/on.png); background-color: transparent;}");
        }else
        {
            m_indicators.at(i)->setStyleSheet("QPushButton {border-image: url(:/Icons/off.png); background-color: transparent;}");
        }
    }

    if(m_clientGameState->isSomeoneSaidCambio())
    {
        int index = m_clientGameState->indexOfPlayerWhoSaidCambio();
        int numOfCards = m_clientGameState->numOfCardsOfEachPlayer().at(index);

        for(int i = 0; i < m_clientGameState->numOfPlayers(); i++)
        {
            if((i + m_clientGameState->myIndex()) % m_clientGameState->numOfPlayers() == index)
            {
                m_avatars.at(i)->setDisabled(true);
                m_avatars.at(i)->setStyleSheet("QPushButton {border-image: url(:/Icons/lock.png); background-color: transparent;}");
                m_avatars.at(i)->show();

                auto *layout = qobject_cast<QGridLayout *>(m_frames.at(i)->layout());

                for(int j = 0; j < 2; j++)
                {
                    for(int k=0; k < (numOfCards % 2 == 0 ? numOfCards / 2 : numOfCards / 2 + 1); k++)
                    {
                        QLayoutItem *item = layout->itemAtPosition(j, k);

                        QPushButton *btnCard = nullptr;

                        if(item && item->widget())
                        {
                            btnCard = qobject_cast<QPushButton *>(item->widget());
                            btnCard->setStyleSheet("QPushButton {border-image: url(:/Cards/Back/cardBack.png); border-radius: 10px; background-color:transparent;}");
                            btnCard->setDisabled(true);
                        }
                    }
                }
            }
        }
    }
}

void GameGui::onTbtnGameRoomSoundClicked()
{
    if (m_volume)
    {
        m_audioOutput->setMuted(true);
        QPixmap pixmap(":/Icons/noSound.png");
        QIcon buttonIcon(pixmap);
        ui->tbtnGameRoomSound->setIcon(buttonIcon);
        ui->tbtnGameRoomSound->setIconSize(QSize(35, 35));

    }
    else
    {
        m_audioOutput->setMuted(false);
        QPixmap pixmap(":/Icons/sound.png");
        QIcon buttonIcon(pixmap);
        ui->tbtnGameRoomSound->setIcon(buttonIcon);
        ui->tbtnGameRoomSound->setIconSize(QSize(35, 35));

    }
    m_volume = !m_volume;
}

void GameGui::onTbtnGameGuideClicked()
{
    Rules *rules = new Rules();
    rules->show();
}

void GameGui::initializeMainPlayerFrame()
{
    int frameWidth = 150;
    int frameHeight = 220;
    auto *frame = new QFrame(this);
    frame->setGeometry(m_screenWidth / 2 - frameWidth / 2, m_screenHeight - frameHeight,
                       frameWidth, frameHeight);
    frame->setStyleSheet("QFrame {background-color: transparent}"
                         "{background: none}"
                         "{border: none}"
                         "{background-repeat: none}");
    frame -> setLayout(new QGridLayout);


    int frameXPosition = frame->geometry().x();
    int frameYPosition = frame->geometry().y();

    auto *indicator = new QPushButton(this);
    int indicatorSize = 40;
    indicator->setGeometry(frameXPosition + frameWidth + 20, frameYPosition + frameHeight / 2 - 10,
                           indicatorSize, indicatorSize);
    if(!m_clientGameState->isMyTurn()){
        indicator->setStyleSheet("QPushButton {border-image: url(:/Icons/off.png); background-color: transparent;}");
    }else{
        indicator->setStyleSheet("QPushButton {border-image: url(:/Icons/on.png); background-color: transparent;}");
    }
    indicator->setDisabled(true);


    auto *name = new QLabel(this);
    int nameSize = 100;
    name->setGeometry(frameXPosition - nameSize - 30, frameYPosition + frameHeight - 50,
                      nameSize, 40);
    name->setText(m_clientGameState->namesOfPlayers().at(m_clientGameState->myIndex()));
    QFont font("Ubuntu Condensed", 15, QFont::Bold);
    name->setFont(font);
    name->setStyleSheet("QLabel {background-color: transparent}"
                        "{background: none}"
                        "{border: none}"
                        "{background-repeat: none}"
                        "{color: rgb(255, 255, 255)}");
    name->setAlignment(Qt::AlignCenter);
    name->setDisabled(true);


    auto *avatar = new QPushButton(this);
    int avatarSize = 100;
    avatar->setGeometry(frameXPosition - avatarSize - 30, frameYPosition + 50,
                        avatarSize, avatarSize);
    QString ss = QStringLiteral("QPushButton {border-image: url(:/Icons/avatar%1.png); background-color: transparent;}").arg(m_clientGameState->avatarsOfPlayers().at(m_clientGameState->myIndex()));
    avatar->setStyleSheet(ss);


    m_frames.push_back(frame);
    m_indicators.push_back(indicator);
    m_names.push_back(name);
    m_avatars.push_back(avatar);
}

void GameGui::initializeUpFrame()
{
    int frameWidth = 150;
    int frameHeight = 220;
    auto *frame = new QFrame(this);
    frame->setGeometry(m_screenWidth / 2 - frameWidth /2, 0,
                       frameWidth, frameHeight);
    frame->setStyleSheet("QFrame {background-color: transparent}"
                         "{background: none}"
                         "{border: none}"
                         "{background-repeat: none}");
    frame -> setLayout(new QGridLayout);

    int frameXPosition = frame->geometry().x();
    int frameYPosition = frame->geometry().y();

    auto *indicator = new QPushButton(this);
    int indicatorSize = 40;
    indicator->setGeometry(frameXPosition + frameWidth + 20, frameYPosition + frameHeight / 2 - 10,
                           indicatorSize, indicatorSize);
    indicator->setDisabled(true);


    auto *name = new QLabel(this);
    int nameSize = 100;
    name->setGeometry(frameXPosition - nameSize - 30, frameYPosition + frameHeight - 50,
                      nameSize, 40);
    QFont font("Ubuntu Condensed", 15, QFont::Bold);
    name->setFont(font);
    name->setStyleSheet("QLabel {background-color: transparent}"
                        "{background: none}"
                        "{border: none}"
                        "{background-repeat: none}"
                        "{color: rgb(255, 255, 255)}");
    name->setAlignment(Qt::AlignCenter);
    name->setDisabled(true);


    auto *avatar = new QPushButton(this);
    int avatarSize = 100;
    avatar->setGeometry(frameXPosition - avatarSize - 30, frameYPosition + 50,
                        avatarSize, avatarSize);


    m_frames.push_back(frame);
    m_indicators.push_back(indicator);
    m_names.push_back(name);
    m_avatars.push_back(avatar);
}

void GameGui::initializeLeftFrame()
{
    int frameWidth = 150;
    int frameHeight = 220;
    auto *frame = new QFrame(this);
    frame->setGeometry(140 , m_screenHeight / 2 - frameHeight / 2,
                       frameWidth, frameHeight);
    frame->setStyleSheet("QFrame {background-color: transparent}"
                         "{background: none}"
                         "{border: none}"
                         "{background-repeat: none}");
    frame -> setLayout(new QGridLayout);

    int frameXPosition = frame->geometry().x();
    int frameYPosition = frame->geometry().y();

    auto *indicator = new QPushButton(this);
    int indicatorSize = 40;
    indicator->setGeometry(frameXPosition + frameWidth + 20, frameYPosition + frameHeight / 2 - 10,
                           indicatorSize, indicatorSize);
    indicator->setDisabled(true);


    auto *name = new QLabel(this);
    int nameSize = 100;
    name->setGeometry(frameXPosition - nameSize - 30, frameYPosition + frameHeight - 50,
                      nameSize, 40);
    QFont font("Ubuntu Condensed", 15, QFont::Bold);
    name->setFont(font);
    name->setStyleSheet("QLabel {background-color: transparent}"
                        "{background: none}"
                        "{border: none}"
                        "{background-repeat: none}"
                        "{color: rgb(255, 255, 255)}");
    name->setAlignment(Qt::AlignCenter);
    name->setDisabled(true);


    auto *avatar = new QPushButton(this);
    int avatarSize = 100;
    avatar->setGeometry(frameXPosition - avatarSize - 30, frameYPosition + 50,
                        avatarSize, avatarSize);


    m_frames.push_back(frame);
    m_indicators.push_back(indicator);
    m_names.push_back(name);
    m_avatars.push_back(avatar);

}

void GameGui::initializeRightFrame()
{
    int frameWidth = 150;
    int frameHeight = 220;
    auto *frame = new QFrame(this);
    frame->setGeometry(m_screenWidth - frameWidth - 140, m_screenHeight / 2 - frameHeight / 2,
                       frameWidth, frameHeight);
    frame->setStyleSheet("QFrame {background-color: transparent}"
                         "{background: none}"
                         "{border: none}"
                         "{background-repeat: none}");
    frame -> setLayout(new QGridLayout);

    int frameXPosition = frame->geometry().x();
    int frameYPosition = frame->geometry().y();

    auto *indicator = new QPushButton(this);
    int indicatorSize = 40;
    indicator->setGeometry(frameXPosition - indicatorSize - 20, frameYPosition + frameHeight / 2 - 10,
                           indicatorSize, indicatorSize);
    indicator->setDisabled(true);


    auto *name = new QLabel(this);
    int nameSize = 100;
    name->setGeometry(frameXPosition + frameWidth + 30, frameYPosition + frameHeight - 50,
                      nameSize, 40);
    QFont font("Ubuntu Condensed", 15, QFont::Bold);
    name->setFont(font);
    name->setStyleSheet("QLabel {background-color: transparent}"
                        "{background: none}"
                        "{border: none}"
                        "{background-repeat: none}"
                        "{color: rgb(255, 255, 255)}");
    name->setAlignment(Qt::AlignCenter);
    name->setDisabled(true);


    auto *avatar = new QPushButton(this);
    int avatarSize = 100;
    avatar->setGeometry(frameXPosition + frameWidth + 30, frameYPosition + 50,
                        avatarSize, avatarSize);


    m_frames.push_back(frame);
    m_indicators.push_back(indicator);
    m_names.push_back(name);
    m_avatars.push_back(avatar);
}

void GameGui::initializeOpponentFrames()
{
    if(m_clientGameState->numOfPlayers() == 2)
    {
        initializeUpFrame();
    }
    else if(m_clientGameState->numOfPlayers() == 3)
    {
        initializeLeftFrame();
        initializeUpFrame();
    }
    else
    {
        initializeLeftFrame();
        initializeUpFrame();
        initializeRightFrame();
    }
}
