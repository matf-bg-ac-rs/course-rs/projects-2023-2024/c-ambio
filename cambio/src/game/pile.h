#ifndef PILE_H
#define PILE_H

#include "card.h"
#include <QStack>

class Pile
{
public:
    Pile();

    ~Pile();

    QStack<Card*>& pileOfCards();

    size_t pileSize() const;
    bool isEmpty() const;

    Card* getFirstCard();

    void addCardToPile(Card* card);

    Card* peekFirstCard() const;


private:
    QStack<Card *> m_pileOfCards;
};

#endif // PILE_H
