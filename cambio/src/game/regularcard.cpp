#include "regularcard.h"
#include "card.h"

RegularCard::RegularCard() = default;

RegularCard::RegularCard(int cardNum, CARD_COLOR color)
    : Card(cardNum),
    m_cardColor(color)
{
    if(cardNum >= 7 && cardNum <= 10)
    {
        throw "Numbers between 7 and 10 do not apply to regular cards!";
    }
}

RegularCard::~RegularCard() = default;

auto RegularCard::cardColor() const -> RegularCard::CARD_COLOR { return m_cardColor; }

auto RegularCard::toString() const -> QString
{
    //reg:pink:5
    QString result = "";
    QString cardType = "reg";
    result.append(cardType);
    result.append(":");
    QString color;

    switch (m_cardColor)
    {
    case CARD_COLOR::PINK:
        color = "pink";
        break;
    case CARD_COLOR::BLUE:
        color = "blue";
        break;
    default:
        throw "Unknown";
    }
    result.append(color);
    result.append(":");
    result.append(QString::number(Card::cardNum()));

    return result;
}

QString RegularCard::cardType() const
{
    return "Numeric";
}
