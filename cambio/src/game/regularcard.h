#ifndef REGULARCARD_H
#define REGULARCARD_H

#include "card.h"

class RegularCard : public Card
{
public:
    enum class CARD_COLOR
    {
        PINK,
        BLUE
    };

    RegularCard();
    RegularCard(int num, CARD_COLOR color = RegularCard::CARD_COLOR::PINK);
    ~RegularCard();

    CARD_COLOR cardColor() const;
    //void setCardColor(CARD_COLOR cc);
    //void play(void *) override;
    QString toString() const override;
    QString cardType() const override;

private:
    CARD_COLOR m_cardColor;
};

#endif // REGULARCARD_H
