#include "player.h"

#include <iostream>
#include <utility>


Player::Player() = default;

Player::Player(const QString& name)
    : m_name(name)
    , m_score(0)
    , m_saidCambio(false)
{

}
//getters
bool Player::myTurn() const
{
    return m_myTurn;
}
bool Player::saidCambio() const
{
    return m_saidCambio;
}
int Player::indexInGame() const
{
    return m_indexInGame;
}
int Player::numOfCards() const
{
    int num = 0;
    for (int i = 0; i < m_hand.size(); i++){
        if(m_hand[i] != nullptr)
        {
            num++;
        }
    }
    return num;
}
QString Player::name() const
{
    return m_name;
}
int Player::score() const
{
    return m_score;
}

//setters
void Player::setName(const QString &newName)
{
    m_name = newName;
}
void Player::setScore(const int newScore)
{
    m_score = newScore;
}
void Player::setMyTurn(const bool newMyTurn)
{
    m_myTurn = newMyTurn;
}
void Player::sayCambio(const bool cambio)
{
    m_saidCambio = cambio;
}
void Player::setIndexInGame(const int newIndex )
{
    m_indexInGame = newIndex;
}
void Player::hasPlayed(const bool newHasPlayed)
{
    m_hasPlayed  = newHasPlayed;
}
void Player::setHand(const QVector<Card*> &newHand)
{
    while(m_hand.count())
        delete m_hand.takeLast();
    m_hand = newHand;
}

Card *Player::peekCardAtIndex(const int indexOfCard) const
{
    return m_hand[indexOfCard];
}

//methods
void Player::addToHand(Card* card)
{
    for(int i = 0; i<m_hand.size(); i++)
    {
        if(m_hand[i] == nullptr)
        {
            m_hand[i] = card;
            break;
        }
    }
    m_hand.push_back(card);
}

Card* Player::changeCardAtIndex(Card* card, const int index)
{
    Card* oldCard = m_hand[index];
    m_hand[index] = card;
    return oldCard;
}

QVector<Card *> Player::hand() const
{
    return m_hand;
}
void Player::calculateScore()
{
    int suma = 0;
    for (int i = 0; i < m_hand.size(); i++) {
        Card *pom = m_hand[i];
        if(pom != nullptr)
        {
            suma += pom->cardNum();
        }
    }
    setScore(suma);
}



