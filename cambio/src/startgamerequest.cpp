#include "startgamerequest.h"

StartGameRequest::StartGameRequest(const bool isHost, const QString &name, const int indexInGame)
    : Packet("start game request")
    , m_isHost(isHost)
    , m_name(name)
    , m_indexInGame(indexInGame)
{
}

QString StartGameRequest::name() const
{
    return m_name;
}

bool StartGameRequest::isHost() const
{
    return m_isHost;
}

QByteArray StartGameRequest::toSend() const
{
    QJsonObject jsonObj;
    jsonObj.insert("type", "start game request");
    jsonObj.insert("name", m_name);
    jsonObj.insert("isHost", m_isHost);
    jsonObj.insert("indexInGame", m_indexInGame);

    QJsonDocument doc;
    doc.setObject(jsonObj);
    return doc.toJson();
}

int StartGameRequest::indexInGame() const
{
    return m_indexInGame;
}
